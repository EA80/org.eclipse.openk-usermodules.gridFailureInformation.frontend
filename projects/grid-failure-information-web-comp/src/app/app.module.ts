/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';

import { AppComponent } from './app.component';
import { MapModule } from '@grid-failure-information-map-app/app/app.module';
import { createCustomElement } from '@angular/elements';
import { MapComponent } from '@grid-failure-information-map-app/app/app.component';
import { TableModule } from '@grid-failure-information-table-app/app/app.module';
import { TableComponent } from '@grid-failure-information-table-app/app/app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    MapModule.forRoot(),
    TableModule.forRoot(),
  ],
  providers: [],
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const ngElementMap = createCustomElement(MapComponent, {
      injector: this.injector,
    });
    customElements.define('openk-grid-failure-information-map-comp', ngElementMap);

    const ngElementTable = createCustomElement(TableComponent, {
      injector: this.injector,
    });
    customElements.define('openk-grid-failure-information-table-comp', ngElementTable);


  }

}
