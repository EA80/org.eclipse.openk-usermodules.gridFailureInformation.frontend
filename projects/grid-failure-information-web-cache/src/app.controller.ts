/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  Controller,
  Get,
  Post,
  Body,
  Res,
  HttpStatus,
  Req,
} from '@nestjs/common';
import { AppService } from './app.service';
import { Response, Request } from 'express';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post('/internal-sit')
  createPublicSITs(@Req() req: Request, @Res() res: Response): void {
    // console.log(res);
    try {
      this.appService.saveGridFailures(
        req.body,
        (isSuccesFullSave: boolean, message: string) => {
          res.statusMessage = message;
          isSuccesFullSave && res.status(HttpStatus.CREATED).send();
        },
      );
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      res.send();
    }
  }

  @Post('/fe-settings')
  createPublicSettings(@Req() req: Request, @Res() res: Response): void {
    // console.log(res);
    try {
      this.appService.saveSettings(
        req.body,
        (isSuccesFullSave: boolean, message: string) => {
          res.statusMessage = message;
          isSuccesFullSave && res.status(HttpStatus.CREATED).send();
        },
      );
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      res.send();
    }
  }

  @Get('/public-sit')
  getSITs(@Res() res: Response) {
    try {
      this.appService.getSITs((data: any) => {
        if (!!data && !!data.length) {
          res.status(HttpStatus.OK);
          res.send(data);
        } else {
          res.statusMessage = 'File is empty of file not found!';
          res.status(HttpStatus.NOT_FOUND);
          res.send(data);
        }
      });
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      res.send([]);
    }
  }

  @Get('/public-settings')
  getSettings(@Res() res: Response) {
    try {
      this.appService.getSettings((data: any) => {
        if (!!data && !!data.length) {
          res.status(HttpStatus.OK);
          res.send(data);
        } else {
          res.statusMessage = 'File is empty of file not found!';
          res.status(HttpStatus.NOT_FOUND);
          res.send(data);
        }
      });
    } catch (error) {
      res.status(HttpStatus.INTERNAL_SERVER_ERROR);
      res.send([]);
    }
  }
}
