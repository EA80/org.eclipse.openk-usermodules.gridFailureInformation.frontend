/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnDestroy } from '@angular/core';
import { AgFilterComponent } from 'ag-grid-angular';
import { IDoesFilterPassParams, IFilterParams, RowDataChangedEvent, RowNode } from 'ag-grid-community';
import { convertEmptyValueToNull } from '@grid-failure-information-app/shared/utility/utilityHelpers';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'filter-cell',
  templateUrl: './set-filter.component.html',
  styleUrls: ['./set-filter.component.css'],
})
export class SetFilterComponent implements AgFilterComponent, OnDestroy {
  public setItems: any = {};
  public gridHeight: number;
  public filterText: string = '';
  public selectAllChecked: boolean = true;
  public isQuickfilterPublishChecked: boolean = false;
  public publisherFilterIsActive: boolean = false;

  private _filteredItems: any = {};
  private _params: IFilterParams;
  protected _valueGetter: (rowNode: RowNode) => any;
  private _filterId: string = '';

  protected _endSubscriptions$: Subject<boolean> = new Subject();

  filterCheckboxList(text: string) {
    var filterWord = text.toLowerCase();
    const trimmedFilterWord = filterWord;
    if (Object.keys(this.setItems).length > 0) {
      // setItems allready there ??
      for (var itemKey in this.setItems) {
        if (this._contains(itemKey, trimmedFilterWord)) {
          // filtertext is in itemKey ??
          this._extendFilteredItem(trimmedFilterWord);
        } else {
          this._filteredItems[itemKey] = this.setItems[itemKey];
          delete this.setItems[itemKey];
        }
      }
    } else {
      this._extendFilteredItem(trimmedFilterWord);
    }
    if (this.filterText.length === 0) {
      this.selectAll();
    } else {
      this.selectAllChecked = false;
    }

    this._params.filterChangedCallback();
  }

  private _extendFilteredItem(filterWord: string): void {
    for (var filteredItem in this._filteredItems) {
      if (this._contains(filteredItem, filterWord)) {
        this.setItems[filteredItem] = this._filteredItems[filteredItem];
      }
    }
  }

  private _contains(sourceText: string, subString: string): boolean {
    return sourceText.toString().toLowerCase().indexOf(subString.toString().toLowerCase()) >= 0;
  }

  notifyFilter(event: Event) {
    let selectedFilter = event.target['id'];
    this.setItems[selectedFilter].checked = event.target['checked'];
    let allSelected: boolean = this._areAllFilterItemsSelected();
    this.selectAllChecked = allSelected && !!this.filterText && this.filterText.length === 0;
    this._params.filterChangedCallback({ filterId: this._filterId });
  }

  isFilterActive(): boolean {
    this.selectAllChecked = this._areAllFilterItemsSelected();
    return !this.selectAllChecked;
  }

  doesFilterPass(params: IDoesFilterPassParams): boolean {
    const itemKey = this._valueGetter(params.node);
    return this.setItems[itemKey || null] !== undefined && this.setItems[itemKey || null].checked;
  }

  getModel(): any {
    return { values: this.setItems };
  }

  setModel(model: any): void {
    this.setItems = model ? model.values : [];
    this.selectAllChecked = !model;
  }

  afterGuiAttached(params?: any) {
    if (!!this.filterText && this.filterText.trim().length > 0) {
      this.filterCheckboxList(this.filterText.trim());
    }
    this._setFilterItems(params);
  }

  agInit(params: IFilterParams): void {
    this._params = params;
    this._filterId = params.colDef.colId;
    this._valueGetter = params.valueGetter;
    this._setFilterItems(params);
    this._params.api = !this._params.api ? ({ gridOptionsWrapper: { gridOptions: {} } } as any) : this._params.api;
    this._params.api['gridOptionsWrapper'].gridOptions = {
      ...this._params.api['gridOptionsWrapper'].gridOptions,
      onGridSizeChanged: (event: any) => {
        this.gridHeight = event.clientHeight;
      },
      onRowDataChanged: (event: RowDataChangedEvent) => {
        this.filterText = null;
        this._setFilterItems(params);
      },
    };
  }

  ngOnDestroy(): void {
    this._endSubscriptions$.next(true);
  }

  private _setFilterItems(params: IFilterParams) {
    const allGridRowNodes = (this._params.rowModel as any).nodeManager.allNodesMap || {};
    const oldFilterItems = {};
    Object.assign(oldFilterItems, this.setItems);
    if (Object.keys(allGridRowNodes).length > 0) {
      this.setItems = {};
      for (const nodeKey in Object.keys(allGridRowNodes)) {
        const rowNode = allGridRowNodes[nodeKey];
        if (this._params.doesRowPassOtherFilter(rowNode)) {
          let filterItem = convertEmptyValueToNull(this._valueGetter(rowNode));
          const valueSeparator: string = this._params.colDef['valueSeparator'];
          if (!!filterItem && !!valueSeparator) {
            const filterItems: string[] = filterItem.split(valueSeparator);
            filterItems.forEach(item => {
              this._addItemToFilterModel(oldFilterItems, item);
            });
          } else {
            this._addItemToFilterModel(oldFilterItems, filterItem);
          }
        }
      }
    }
    this.selectAllChecked = this._areAllFilterItemsSelected();
    this._params.filterChangedCallback();
  }

  private _addItemToFilterModel(oldFilterItems: {}, item: string) {
    const isFilterItemInOldFilter = oldFilterItems.hasOwnProperty(item);
    let checked: boolean = (!isFilterItemInOldFilter && this.selectAllChecked) || (isFilterItemInOldFilter && oldFilterItems[item]['checked'] === true);
    const itemValueObject = { visible: true, checked: checked };
    if (!!this.filterText && this.filterText.trim().length > 0) {
      if (!!item && this._contains(item, this.filterText.trim())) {
        this.setItems[item] = itemValueObject;
      }
    } else {
      this.setItems[item] = itemValueObject;
    }
  }

  public selectAll(event?: Event) {
    this.selectAllChecked = event ? event.target['checked'] : true;
    this.filterText = '';
    for (var itemKey in this._filteredItems) {
      this.setItems[itemKey] = this._filteredItems[itemKey];
    }
    for (var itemKey in this.setItems) {
      if (this.setItems.hasOwnProperty(itemKey)) {
        this.setItems[itemKey].checked = this.selectAllChecked;
      }
    }
    this._params.filterChangedCallback();
  }

  private _areAllFilterItemsSelected(): boolean {
    if (!!this.filterText && this.filterText.trim().length > 0) {
      return false;
    }
    for (var itemKey in this.setItems) {
      if (!this.setItems[itemKey].checked) {
        return false;
      }
    }
    return true;
  }

  constructor() {}
}
