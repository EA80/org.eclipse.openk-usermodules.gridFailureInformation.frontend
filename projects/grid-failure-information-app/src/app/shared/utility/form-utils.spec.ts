/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as formUtils from '@grid-failure-information-app/shared/utility/form-utils';
import { box } from 'ngrx-forms';

describe('forms utils', () => {
  it('should can map a contructor with boxed properties', () => {
    const data = { normal_property: '123', boxed_property: box('456') } as any;
    const map_data = formUtils.unboxProperties(data);
    expect(map_data).toEqual({ normal_property: '123', boxed_property: '456' });
  });
});
