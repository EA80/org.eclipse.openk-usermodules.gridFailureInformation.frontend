/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Router } from '@angular/router';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { DateTimeModel } from '@grid-failure-information-app/shared/models/date-time.model';
import { NgrxValueConverter, NgrxValueConverters } from 'ngrx-forms';
import { FailureHousenumber } from '@grid-failure-information-app/shared/models';
import { VisibilityConfigurationInterface } from '@grid-failure-information-app/shared/interfaces/visibility-configuration.interface';
import { VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';

/**
 * Returns formated date based on given culture
 *
 * @param dateString
 * @param culture
 */
export function localeDateString(dateString: string, culture: string = 'de-DE'): string {
  let date = new Date(dateString);
  return date.toLocaleDateString(culture);
}

/**
 * Convert a date object into an ISO-String format
 */
export const dateTimePickerValueConverter: NgrxValueConverter<any | null | DateTimeModel, string | any | null> = {
  convertViewToStateValue(value) {
    if (value === null) {
      return null;
    }

    let date = new Date(Date.UTC(value.year, value.month - 1, value.day, value.hour, value.minute));

    return NgrxValueConverters.dateToISOString.convertViewToStateValue(date);
  },
  convertStateToViewValue(value) {
    if (value === null) {
      return null;
    }

    let date = new Date(Date.UTC(value.year, value.month - 1, value.day, value.hour, value.minute));
    return padNumber(value.day) + '.' + padNumber(value.month) + '.' + value.year + ' / ' + padNumber(value.hour) + ':' + padNumber(value.minute);
  },
};

export function navigateHome(router: Router): Promise<boolean> {
  return router.navigateByUrl(`/grid-failures`);
}

export function toInteger(value: any): number {
  return parseInt(`${value}`, 10);
}

export function isNumber(value: any): value is number {
  return !isNaN(toInteger(value));
}

export function padNumber(value: number) {
  if (isNumber(value)) {
    return `0${value}`.slice(-2);
  } else {
    return '';
  }
}

/**
 * Convert a station object to station description
 */
export const stationToStationDescriptionConverter: NgrxValueConverter<any | null, string | null> = {
  convertViewToStateValue(value) {
    if (value === null) {
      return null;
    }

    if (value.stationName) return value.stationName + ' (' + value.stationId + ')';
    else return value;
  },
  convertStateToViewValue(value) {
    return value;
  },
};

export function dateTimeComparator(filterLocalDateAtMidnight: Date, cellValue: string): number {
  if (cellValue == null) return -1;
  const cellDate: Date = new Date(cellValue.replace('Z', ''));
  cellDate.setHours(0, 0, 0, 0);

  const filterLocalDateAtMidnightTimeStamp = filterLocalDateAtMidnight.getTime();
  const cellDateTimeStamp = cellDate.getTime();

  if (filterLocalDateAtMidnightTimeStamp === cellDateTimeStamp) {
    return 0;
  }
  if (cellDateTimeStamp < filterLocalDateAtMidnightTimeStamp) {
    return -1;
  }
  if (cellDateTimeStamp > filterLocalDateAtMidnightTimeStamp) {
    return 1;
  }
}

export function convertEmptyValueToNull(value: string): string {
  if (value === null || value.toString().trim() === '') {
    return null;
  } else return value;
}

export function getDependencyPropertyFromControlId(controlId: string): string {
  return controlId.substring(controlId.indexOf('.') + 1, controlId.length);
}

export function getAddressRelevantBranch(branch: string): string {
  const addressRelevantBranches: string[] = [Globals.BUSINESS_RULE_FIELDS.branch.power];
  return addressRelevantBranches.find(b => b === branch);
}

export function sortAlphaNum(firstInput: any, secondInput: any) {
  if (firstInput instanceof FailureHousenumber && secondInput instanceof FailureHousenumber) {
    firstInput = firstInput.housenumber;
    secondInput = secondInput.housenumber;
  }

  if (firstInput === secondInput) return 0;
  if (firstInput === null || (firstInput === '' && !!secondInput)) return -1; // handle ascending sorting with NULL values
  if (secondInput === null || (secondInput === '' && !!firstInput)) return 1; // handle decending sorting with NULL values

  // NULL or UNDEFINED handling
  firstInput = !firstInput ? '0' : String(firstInput);
  secondInput = !secondInput ? '0' : String(secondInput);

  const removeA = /[^a-zA-Z]/g;
  const removeNumber = /[^0-9]/g;
  const firstInputNumber = parseInt(firstInput.replace(removeNumber, /^.+-/, /^.+ /, ''));
  const secondInputNumber = parseInt(secondInput.replace(removeNumber, /^.+-/, /^.+ /, ''));
  if (firstInputNumber === secondInputNumber) {
    const firstInputA = firstInput.replace(removeA, '');
    const secondInputA = secondInput.replace(removeA, '');
    return firstInputA === secondInputA ? 0 : firstInputA > secondInputA ? 1 : -1;
  } else {
    return firstInputNumber > secondInputNumber ? 1 : -1;
  }
}
export function sortItems(actionPayload: any) {
  const sortedItems = actionPayload.payload.sort((a, b) => a.localeCompare(b));
  actionPayload = { ...actionPayload, payload: sortedItems };
  return actionPayload;
}

export function determineDetailFieldVisibility(config: VisibilityConfigurationInterface, config_prop: string, field: string): boolean {
  if (config && config[config_prop]) {
    return config[config_prop][field] === VisibilityEnum.SHOW;
  } else {
    return true;
  }
}
