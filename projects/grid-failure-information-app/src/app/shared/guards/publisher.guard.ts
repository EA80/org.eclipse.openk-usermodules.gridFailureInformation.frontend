/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import * as store from '@grid-failure-information-app/shared/store';
import { User } from '@grid-failure-information-app/shared/models/user';
import { PermissionsModel } from '@grid-failure-information-app/shared/models/permissions.model';
import { takeUntil, take, map } from 'rxjs/operators';

@Injectable()
export class PublisherGuard implements CanActivate {
  protected _endSubscriptions$: Subject<boolean> = new Subject();
  constructor(private _router: Router, private _appState$: Store<store.State>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._checkPublisherRight();
  }

  private _checkPublisherRight(): Observable<boolean> {
    const isPublisher$: Observable<PermissionsModel> = this._appState$
      .select(store.getUser)
      .pipe(takeUntil(this._endSubscriptions$), take(1))
      .map((user: User) => new PermissionsModel(user.roles));

    isPublisher$.subscribe((permission: PermissionsModel) => {
      !permission.publisher && !permission.admin && this._router.navigate(['/grid-failures']);
    });
    return isPublisher$.pipe(
      map((permission: PermissionsModel) => !!permission.publisher || !!permission.admin),
      take(1)
    );
  }
}
