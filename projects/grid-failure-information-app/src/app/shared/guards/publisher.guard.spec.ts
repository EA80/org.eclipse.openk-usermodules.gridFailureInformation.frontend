/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { PublisherGuard } from '@grid-failure-information-app/shared/guards/publisher.guard';
import { of } from 'rxjs/observable/of';
import { User } from '@grid-failure-information-app/shared/models/user';

describe('PublisherGuard', () => {
  let component: PublisherGuard;
  let appState: any;
  let router: any;

  beforeEach(() => {
    appState = {
      pipe: () => of(1),
      dispatch: () => {},
      select: () => of(1),
    };
    router = {
      navigate() {},
    } as any;

    component = new PublisherGuard(router, appState);
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should call _checkPublisherRight wenn calling canActivate', () => {
    const spy = spyOn(component as any, '_checkPublisherRight');
    component.canActivate({} as any, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should check if has publisher right', () => {
    const user: User = new User();
    user.id = '123';
    user.roles = ['publisher', 'reader'];

    const spy = spyOn(appState, 'select').and.returnValue(of(user));
    const spy2 = spyOn(router, 'navigate');
    (component as any)._checkPublisherRight();
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalledWith(['/grid-failures']);
  });
});
