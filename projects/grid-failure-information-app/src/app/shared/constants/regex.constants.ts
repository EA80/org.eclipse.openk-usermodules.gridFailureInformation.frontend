/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Regular expression to use for validating digits only fields
 */
export const DIGITS_REGEXP: RegExp = /^[0-9]+$/;

/**
 * Regular expression to use for validating phone number fields
 */
export const PHONE_NUMBER_REGEXP: RegExp = /^[0-9\+\-/ ]+$/;

/**
 * Regular expression to use for validating personal name fields
 */
export const PERSONAL_NAME_REGEXP: RegExp = /^[a-zA-Z\s\-`äöüÄÖÜß]+$/;

/**
 * Regular expression to use for validating academic title fields
 */
export const ACAD_TITLE_REGEXP: RegExp = /[^0-9]+$/;

/**
 * Regular expression to use for validating po box fields
 */
export const PO_BOX_REGEXP: RegExp = /^[0-9a-zA-ZäöüÄÖÜß]+$/;

/**
 * Regular expression to use for validating location, city, street name fields
 */
export const LOCATION_NAME_REGEXP: RegExp = /^[0-9a-zA-ZäöüÄÖÜß\-.\/ ]+$/;

/**
 * Regular expression to use for validating street numbers.
 * Allowed chars are 0-9, a-z, A-Z, äöüÄÖÜ.
 */
export const STREET_NUMBER_REGEXP: RegExp = /^[0-9a-zA-ZäöüÄÖÜß\-\/ ]+$/;

/**
 * Regular expression to use for validating zip code fields
 */
export const ZIP_CODE_REGEXP: RegExp = /^[0-9a-zA-zäöüÄÖÜß\-/^°!"§$%&()=?`\{\[\]\}´`+*~#',.;:_@€µ<>| ]+$/;

/**
 * Regular expression to validate a number string to contain only comma (where normally a dot is) with max 2 decimals.
 */
export const NUMBER_WITH_COMMA_TWO_DECIMALS: RegExp = /^\d+[,]?\d{0,2}$/;

/**
 * Regular expression to validate a number string to contain only comma (where normally a dot is) with decimals without limit.
 */
export const NUMBER_WITH_COMMA_X_DECIMALS: RegExp = /^\d+[,]?\d*$/;

/**
 * Regular expression to validate a number which constists only of integer numbers 0-9.
 * No fraction digits or decimal separators allowed.
 */
export const INTEGER_REGEX: RegExp = /^[0-9]*$/;

/*
 * Regular expression for email addresses.
 * Allowed characters for the local part are only letters, numbers and following special characters: ".", "-", "_".
 * Allowed characters for the domain part are only letters, numbers and following special characters: ".", "-".
 */
export const EMAIL_REGEXP: RegExp = /^(([a-zA-ZäöüÄÖÜ0-9\-_\.])+)@(([a-zA-ZäöüÄÖÜ0-9\-]+\.)+[a-zA-Z]{2,})$/;

/**
 * Regular expression to use for numbers with max 2 decimal places while comma is allowed.
 */
export const COMMA_SEPARATED_NUMBER_WITH_TWO_DECIMAL_REGEXP: RegExp = /^((0|([1-9]\d*))([\,]\d{1,2})?)$/;

/**
 * Regular expression to use for insurant number with a capital letter and 9 digits.
 */
export const INSURANT_NUMBER_REGEXP: RegExp = /^[A-Z][0-9]{9}$/;

/**
 * Numbers with max. 2 decimal places while comma and negative values are allowed.
 */
export const NEGATIVE_AND_COMMA_SEPARATED_NUMBER_WITH_TWO_DECIMAL_REGEXP: RegExp = /^-?((0|([1-9]\d*))([\,]\d{1,2})?)$/;

/**
 * Numbers with max. 2 decimal places while comma and negative values are allowed.
 */
export const MULTI_LINE_TEXT: RegExp = /^[\w.\-,:_';`äöüÄÖÜß+?€!#*\n\s]+$/;

/**
 * Regular expression to use for numbers != 0.00 with max 2 comma seperated decimal places (negative numbers allowed)
 */
export const COMMA_SEPARATED_NUMBER_WITH_TWO_DECIMAL_REGEXP_NOT_ZERO: RegExp = /^[-]?(?=.*[1-9])\d*(?:\,\d{1,2})?\s*$/;

/**
 * Regular expression to use for validating barcodes
 */
export const BARCODE_REGEXP: RegExp = /^[0-9\+\-/ ]+$/;

/**
 * Regular expression to validate a number string to contain only comma
 */
export const COMMA_SEPARATED_NUMBER: RegExp = /^-?\d+,?\d*$/;

/**
 * Regular expression to validate a positive  decimal number string to contain only comma and up to 2 decimal places
 */
export const COMMA_SEPARATED_POSITIVE_NUMBER_WITH_TWO_DECIMAL_REGEXP: RegExp = /^\d+,?\d{0,2}$/;

/**
 * Regular expression to use for numbers with max 2 decimal places while dot is allowed.
 */
export const DOT_SEPARATED_POSITIVE_NUMBER_WITH_TWO_DECIMAL_REGEXP: RegExp = /^\d+\.?\d{0,2}$/;

/**
 * Regular expression to validate against numbers and dots.
 */
export const NUMBERS_AND_DOTS: RegExp = /^[0-9.]+$/;

/**
 * Regular expression to validate against numbers and letters.
 */
export const LETTERS_AND_NUMBERS: RegExp = /^[0-9a-zA-Z]+$/;

/**
 * Regular expression to use for validating the LEGS field
 */
export const LETTERS_AND_NUMBERS_AND_UMLAUTS: RegExp = /^[0-9a-zA-ZäöüÄÖÜß]+$/;

/**
 * Regular expression to validate against digits, plus, minus, slash, space and dot.
 */
export const DIGITS_PLUS_MINUS_SLASH_SPACE_DOT: RegExp = /^[0-9\+\-/ \.]+$/;

/**
 * Regular expression to ckeck password guidelines.
 * - must contain at least one digit
 * - must contain at least one lower ca
 * - must contain at least one upper case
 * - must contain at least one special character, but we are escaping reserved RegEx characters to avoid conflict
 * - must contain at least 8 from the mentioned characters
 */
export const PASSWORD_CHECK_REGEXP: RegExp = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

/**
 * Regular expression to match only digits and special characters (exclude all letters).
 * Allowed symbols are described with intervals based on the ASCII Table.
 */
export const DIGITS_AND_SPECIAL_CHARACTERS: RegExp = /^[!-@[-`{-~]+$/;

/**
 * Regular expression to match a positive number string with dot decimal separator and unlimited digits after the sign
 */
export const POSITIVE_DOT_SEPARATED_NUMBER: RegExp = /^\d+.?\d*$/;

/**
 * Regular expression to match a tax identifaction number
 * as being used in germany.
 */
export const TAX_IDENTIFACTION_NUMBER: RegExp = /^[a-zA-Z]{2}[0-9]{9}$/;

/**
 * Regular expression to match a positive number string with dot or comma decimal separator and unlimited digits after the sign.
 */
export const POSITIVE_DOT_OR_COMMA_SEPARATED_NUMBER: RegExp = /^\d+[,|\.]?\d*$/;

/**
 * Regular expression to match a positive number string with dot or comma decimal separator.
 */
export const POSITIVE_WITH_ONLY_ONE_DOT_OR_COMMA_SEPARATED_NUMBER: RegExp = /^[+]?\d+([,|\.]\d+)?$/;

/**
 * Regular expression to match a UUID version 4.
 */
export const UUID_V4: RegExp = /[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}/;
