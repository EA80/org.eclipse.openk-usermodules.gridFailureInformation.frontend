 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  trigger,
  animate,
  style,
  transition
} from '@angular/animations';

export function fallIn() {
  return trigger('fallIn', [
    transition(':enter', [
      style({ opacity: '0', transform: 'translateY(40px)' }),
      animate(
        '.4s .2s ease-in-out',
        style({ opacity: '1', transform: 'translateY(0)' })
      )
    ]),
    transition(':leave', [
      style({ opacity: '1', transform: 'translateX(0)' }),
      animate(
        '.3s ease-in-out',
        style({ opacity: '0', transform: 'translateX(-200px)' })
      )
    ])
  ]);
}
