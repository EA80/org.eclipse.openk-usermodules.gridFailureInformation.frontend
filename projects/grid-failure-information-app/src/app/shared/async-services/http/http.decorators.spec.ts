/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as decorators from '@grid-failure-information-app/shared/async-services/http/http.decorator';
import { MediaType } from '@grid-failure-information-app/shared/async-services/http/http.service';

describe('Http Decorators', () => {
  it('should return the given url in target if call BaseUrl decorator', () => {
    const url = '/test';
    const targetFunction: any = {
      prototype: {
        getBaseUrl() {
          url;
        },
      },
    };
    const methodObject = decorators.BaseUrl(url)(targetFunction);
    expect(methodObject).toBe(targetFunction);
    expect(methodObject.prototype.getBaseUrl()).toBe(url);
  });

  it('should return the given url in target if call DefaultHeaders decorator', () => {
    const headers: any = { test: 'test', property: 'property' };
    const targetFunction: any = {
      prototype: {
        getDefaultHeaders() {
          headers;
        },
      },
    };
    const methodObject = decorators.DefaultHeaders(headers)(targetFunction);
    expect(methodObject).toBe(targetFunction);
    expect(methodObject.prototype.getDefaultHeaders()).toBe(headers);
  });

  it('should build the descriptor headers as the headersDef if calling Headers decorator', () => {
    const headersDef: any = { test: 'test', property: 'property' };
    const target: any = {};
    const propertyKey = 'key';
    const descriptor: any = { headers: undefined };

    const methodObject = decorators.Headers(headersDef)(target, propertyKey, descriptor);
    expect(methodObject).toBe(descriptor);
    expect(methodObject.headers).toBe(headersDef);
  });

  it('should build the descriptor and assign the JSON flag if calling Produces decorator with a JSON media type', () => {
    const producesDef: any = MediaType.JSON;
    const target: any = {};
    const propertyKey = 'key';
    const descriptor: any = { isJSON: false, isFormData: false };

    const methodObject = decorators.Produces(producesDef)(target, propertyKey, descriptor);
    expect(methodObject).toBe(descriptor);
    expect(methodObject.isJSON).toBeTruthy();
    expect(methodObject.isFormData).toBeFalsy();
  });

  it('should build the descriptor and assign the FORM_DATA flag if calling Produces decorator with a FORM_DATA media type', () => {
    const producesDef: any = MediaType.FORM_DATA;
    const target: any = {};
    const propertyKey = 'key';
    const descriptor: any = { isJSON: false, isFormData: false };

    const methodObject = decorators.Produces(producesDef)(target, propertyKey, descriptor);
    expect(methodObject).toBe(descriptor);
    expect(methodObject.isJSON).toBeFalsy();
    expect(methodObject.isFormData).toBeTruthy();
  });

  it('should build the descriptor and assign him the given adapter if calling Adapter decorator', () => {
    const adapterFn: Function = new Function();
    const target: any = {};
    const propertyKey = 'key';
    const descriptor: any = { adapter: undefined };

    const methodObject = decorators.Adapter(adapterFn)(target, propertyKey, descriptor);
    expect(methodObject).toBe(descriptor);
    expect(methodObject.adapter).toBe(adapterFn);
  });

  it('should build the descriptor and assign him null if calling Adapter decorator without a function', () => {
    const adapterFn: any = undefined;
    const target: any = {};
    const propertyKey = 'key';
    const descriptor: any = { adapter: undefined };

    const methodObject = decorators.Adapter(adapterFn)(target, propertyKey, descriptor);
    expect(methodObject).toBe(descriptor);
    expect(methodObject.adapter).toBe(null);
  });
});
