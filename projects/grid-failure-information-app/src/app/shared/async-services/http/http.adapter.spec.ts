/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { HttpAdapter } from '@grid-failure-information-app/shared/async-services/http/http.adapter';

describe('HttpAdapter', () => {
  beforeEach(() => {});

  it('should call baseAdapter without result if response is not 200', () => {
    const response: any = { status: 999 };
    const result = HttpAdapter.baseAdapter(response);
    expect(result).toBe(undefined);
  });

  it('should call baseAdapter an give the response as result back', () => {
    const response: any = { status: 200 };
    const result = HttpAdapter.baseAdapter(response);
    expect(result).toBe(response);
  });
});
