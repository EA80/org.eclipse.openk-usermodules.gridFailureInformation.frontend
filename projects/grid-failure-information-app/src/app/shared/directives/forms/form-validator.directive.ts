/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Directive, HostBinding, Input } from '@angular/core';
import { FormControlState } from 'ngrx-forms';

/**
 * This validator fills the validation(visualization) gap of ngrx-forms for disabled/dependent fields
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class FormValidatorDirective
 */
@Directive({
  selector: '[required][ngrxFormControlState]',
})
export class FormValidatorDirective {
  @Input() public ngrxFormControlState: FormControlState<any>;
  @Input() public required: boolean;
  @HostBinding('class.ngrx-forms-invalid-directive')
  get invalid(): boolean {
    return this.required && this.ngrxFormControlState.isDisabled && !this.ngrxFormControlState.value;
  }
  @HostBinding('class.ngrx-forms-valid-directive')
  get valid(): boolean {
    return this.required && !!this.ngrxFormControlState.value;
  }

  constructor() {}
}
