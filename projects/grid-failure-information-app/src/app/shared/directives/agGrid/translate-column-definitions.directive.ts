/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Directive, OnInit, Input, OnDestroy } from '@angular/core';
import { AgGridAngular } from 'ag-grid-angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
@Directive({
  selector: '[columnDefs]',
})
export class TranslateColumnDefinitionsDirective implements OnInit, OnDestroy {
  private _columnDefs: any;

  private _languageChangeSubscription: Subscription;

  @Input()
  public set columnDefs(columnDefs: any) {
    this._columnDefs = columnDefs;
    const colDefs = this._getTranslatedColumnDefinitions(columnDefs);
    !!this._agGrid.api && this._agGrid.api.setColumnDefs(colDefs);
  }

  constructor(private _agGrid: AgGridAngular, private _translationService: TranslateService) {}
  ngOnInit() {
    this._agGrid.columnDefs = this._getTranslatedColumnDefinitions(this._columnDefs);
    this._languageChangeSubscription = this._translationService.onLangChange.subscribe(() => {
      const colDefs = this._getTranslatedColumnDefinitions(this._columnDefs);
      this._agGrid.api.setColumnDefs(colDefs);
    });
  }
  public ngOnDestroy() {
    this._languageChangeSubscription.unsubscribe();
  }

  private _getTranslatedColumnDefinitions(columnDefs: any): any {
    if (columnDefs) {
      return columnDefs.map((columnDefinition: any) => {
        return {
          ...columnDefinition,
          headerName: this._translationService.instant(columnDefinition.headerName),
        };
      });
    }
  }
}
