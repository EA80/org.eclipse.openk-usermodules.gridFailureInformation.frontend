/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { AgGridAngular } from 'ag-grid-angular';
import { Directive, OnInit } from '@angular/core';
import { GridOptions, AgGridEvent, Column, ColumnApi } from 'ag-grid-community';

@Directive({ selector: 'ag-grid-angular[autoResizeColumns]' })
export class AutoResizeColumnsDirective implements OnInit {
  private _gridWidth: number;

  constructor(public agGrid: AgGridAngular) {}

  ngOnInit() {
    this._autoResizeColumns();
  }

  private _autoResizeColumns() {
    const gridOptions: GridOptions = {
      ...this.agGrid.gridOptions,
      suppressColumnVirtualisation: true,
      onPaginationChanged: (event: AgGridEvent) => this._onGridViewRendered(event),
      onRowDataChanged: (event: AgGridEvent) => this._onGridViewRendered(event),
      onGridSizeChanged: (event: any) => {
        this._gridWidth = event.clientWidth;
        this._onGridViewRendered(event);
      },
      onColumnVisible: (event: any) => {
        this._onGridViewRendered(event);
      },
    };
    this.agGrid.gridOptions = gridOptions;
  }

  private _onGridViewRendered(event: AgGridEvent) {
    const columnApi: ColumnApi = event.columnApi;
    let totalColumnsWidth = 0;
    let allColumns: Column[] = [];
    let allOffsetColumns: Column[] = [];
    let divider = 1;

    if (!this._gridWidth) {
      return;
    }

    allColumns = columnApi.getAllColumns() || allColumns;

    allColumns = allColumns.filter((column: Column) => column.isVisible());
    columnApi.sizeColumnsToFit(allColumns);
    columnApi.autoSizeColumns(allColumns);
    allOffsetColumns = allColumns.filter((column: Column) => !column.getMaxWidth() && column.isVisible());

    allColumns.forEach((column: Column) => {
      totalColumnsWidth += column.getActualWidth();
    });
    divider = allOffsetColumns.length || 1;

    if (totalColumnsWidth < this._gridWidth) {
      const columnOffset: number = (this._gridWidth - 3 - totalColumnsWidth) / divider;
      allOffsetColumns.forEach((column: Column) => {
        columnApi.setColumnWidth(column.getId(), column.getActualWidth() + columnOffset);
      });
    }
  }
}
