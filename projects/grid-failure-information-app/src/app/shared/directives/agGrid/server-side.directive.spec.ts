/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ServerSideDirective } from '@grid-failure-information-app/app/shared/directives/agGrid/server-side.directive';
import { PaginationComponent } from '@grid-failure-information-app/shared/components/pagination/pagination.component';
import { PageEvent } from '@grid-failure-information-app/shared/models/page-event';
import { PageModel } from '@grid-failure-information-app/shared/models/page/page.model';
import { of } from 'rxjs/observable/of';

describe('ServerSideDirective Test1', () => {
  let component: ServerSideDirective;
  let appState: any;
  let resolver: any;
  let viewContainerRef: any;
  let actionsSubject: any;
  let agGrid: any;

  beforeEach(() => {
    appState = {
      dispatch: () => {},
    };
    actionsSubject = {
      pipe: () => {},
    };
    resolver = {
      resolveComponentFactory: () => {},
    };
    viewContainerRef = {
      createComponent: () => {},
    };
    agGrid = {
      gridOptions: {
        suppressColumnVirtualisation: false,
      },
      api: {
        setRowData: () => {},
      } as any,
    };

    component = new ServerSideDirective(appState, resolver, viewContainerRef, actionsSubject, agGrid);

    component.serverSide = {
      loadAction() {},
      successAction() {},
      pageSize: 200,
    };
  });

  afterEach(() => {
    (component as any)._matPagination = null;
  });

  it('should create an instance', () => {
    expect(component).toBeDefined();
  });

  it('should call resolveComponentFactory if _matPagination not defined', () => {
    (component as any)._matPagination = undefined;

    const pagedItem: PageModel<any> = new PageModel();
    pagedItem.totalElements = 10;
    pagedItem.totalPages = 50;
    pagedItem.content = [];
    pagedItem.pageSize = 50;
    pagedItem.pageable = { pageSize: 50 };

    const spyPipe = spyOn(actionsSubject, 'pipe').and.returnValue(of(pagedItem));
    const spyResolveComponentFactory = spyOn(resolver, 'resolveComponentFactory');

    const thisMatPagination = {
      totalPages: 0,
      length: 0,
      pageSize: 0,
      hidePageSize: undefined,
      page: { subscribe() {} },
    };
    const spyCreateComponent = spyOn(viewContainerRef, 'createComponent').and.returnValue({ instance: thisMatPagination });
    const spyRetrievePage = spyOn(component as any, '_retrievePage');

    component.ngOnInit();

    expect(spyPipe).toHaveBeenCalled();
    expect(spyResolveComponentFactory).toHaveBeenCalled();
    expect(spyCreateComponent).toHaveBeenCalled();
    expect(spyRetrievePage).toHaveBeenCalled();
  });

  it('should not call resolveComponentFactory, set paged properties and call _retrievePage if  _matPagination is defined and call setRowData if api is defined', () => {
    spyOn(appState, 'dispatch').and.callThrough();

    (component as any)._matPagination = {
      totalPages: 0,
      length: 0,
      pageSize: 0,
      hidePageSize: undefined,
      page: { subscribe() {} },
    };

    const pagedItem: PageModel<any> = new PageModel();
    pagedItem.totalElements = 10;
    pagedItem.totalPages = 50;
    pagedItem.content = [];
    pagedItem.pageSize = 50;
    pagedItem.pageable = { pageSize: 50 };

    const spyPipe = spyOn(actionsSubject, 'pipe').and.returnValue(of(pagedItem));
    const spyPage = spyOn((component as any)._matPagination.page, 'subscribe').and.returnValue(new PageEvent());
    const spyResolveComponentFactory = spyOn((component as any)._resolver, 'resolveComponentFactory');
    const spyRetrievePage = spyOn(component as any, '_retrievePage');
    const instance: PaginationComponent = new PaginationComponent();

    const spyCreateComponent = spyOn(viewContainerRef, 'createComponent').and.returnValue({ instance: instance });
    const spySetRowData = spyOn(agGrid.api, 'setRowData');

    component.ngOnInit();

    expect(spyPipe).toHaveBeenCalled();
    expect(spyResolveComponentFactory).not.toHaveBeenCalled();
    expect(spyRetrievePage).toHaveBeenCalled();
    expect(spyCreateComponent).not.toHaveBeenCalled();
    expect(spyPage).toHaveBeenCalled();
    expect(spySetRowData).toHaveBeenCalled();
    expect((component as any)._matPagination.hidePageSize).toBeFalsy();
    expect((component as any)._matPagination.length).toBe(pagedItem.totalElements);
    expect((component as any)._matPagination.pageSize).toBe(pagedItem.pageSize);
    expect((component as any)._matPagination.totalPages).toBe(pagedItem.totalPages);
  });
});

describe('ServerSideDirective Test2', () => {
  let component: ServerSideDirective;
  let appState: any;
  let resolver: any;
  let viewContainerRef: any;
  let actionsSubject: any;
  let agGrid: any;

  beforeEach(() => {
    appState = {
      dispatch: () => {},
    };
    actionsSubject = {
      pipe: () => {},
    };
    resolver = {
      resolveComponentFactory: () => {},
    };
    viewContainerRef = {
      createComponent: () => {},
    };
    agGrid = {
      gridOptions: {
        suppressColumnVirtualisation: false,
      },
      api: undefined,
      rowData: [],
    } as any;

    component = new ServerSideDirective(appState, resolver, viewContainerRef, actionsSubject, agGrid);
  });

  it('should not call resolveComponentFactory, set paged properties and call _retrievePage if  _matPagination is defined and NOT call setRowData if api is undefined and rowdata should equal to content', () => {
    spyOn(appState, 'dispatch');
    component.serverSide = {
      loadAction() {},
      successAction() {},
      pageSize: 200,
    };

    (component as any)._matPagination = {
      totalPages: 0,
      length: 0,
      pageSize: 0,
      hidePageSize: undefined,
      page: { subscribe() {} },
    };

    const pagedItem: PageModel<any> = new PageModel();
    pagedItem.totalElements = 10;
    pagedItem.totalPages = 50;
    pagedItem.content = [{ test: '' }];
    pagedItem.pageSize = 50;
    pagedItem.pageable = { pageSize: 50 };

    const spyPipe = spyOn(actionsSubject, 'pipe').and.returnValue(of(pagedItem));
    const spyPage = spyOn((component as any)._matPagination.page, 'subscribe').and.returnValue(new PageEvent());
    const spyResolveComponentFactory = spyOn((component as any)._resolver, 'resolveComponentFactory');
    const spyRetrievePage = spyOn(component as any, '_retrievePage');
    const instance: PaginationComponent = new PaginationComponent();
    const spyCreateComponent = spyOn(viewContainerRef, 'createComponent').and.returnValue(instance);

    component.ngOnInit();

    expect(spyPipe).toHaveBeenCalled();
    expect(spyResolveComponentFactory).not.toHaveBeenCalled();
    expect((component as any)._matPagination.hidePageSize).toBeFalsy();
    expect((component as any)._matPagination.length).toBe(pagedItem.totalElements);
    expect((component as any)._matPagination.pageSize).toBe(pagedItem.pageSize);
    expect((component as any)._matPagination.totalPages).toBe(pagedItem.totalPages);
    expect(spyRetrievePage).toHaveBeenCalled();
    expect(spyCreateComponent).not.toHaveBeenCalled();

    expect(spyPage).toHaveBeenCalled();
    expect(agGrid.rowData).toBe(pagedItem.content);
  });
});
