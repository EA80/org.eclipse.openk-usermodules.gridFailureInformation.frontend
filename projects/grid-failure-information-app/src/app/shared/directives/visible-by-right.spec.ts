/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { RolesEnum } from '@grid-failure-information-app/shared/constants/enums';
import { VisibleByRightDirective } from '@grid-failure-information-app/shared/directives/visible-by-right';
import { of } from 'rxjs/observable/of';

describe('VisibleByRightDirective', () => {
  let templateRef: any;
  let viewContainerRef: any;
  let appState: any;
  beforeEach(async(() => {
    viewContainerRef = {
      createEmbeddedView: () => {},
      clear: () => {},
      element: { nativeElement: { elements: [{ classList: {}, disabled: false }] } },
    };

    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: [RolesEnum.CREATOR] }),
      map: () => of({ creator: true, publisher: false, admin: false, qualifier: false }),
    };
  }));

  it('should create an instance', () => {
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    expect(directive).toBeTruthy();
  });

  it('should call OnDestroy', () => {
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    const spy = spyOn(directive['_endSubscriptions$'], 'next' as any);
    directive.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it('should create embedded view for admin role', () => {
    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: [RolesEnum.ADMIN] }),
      map: () => of({ creator: false, publisher: false, admin: true, qualifier: false }),
    };
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);

    const spy = spyOn(directive['_viewContainer'], 'createEmbeddedView' as any);
    directive.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should create embedded view for role qualifier', () => {
    appState = {
      pipe: () => of(),
      dispatch: () => {},
      select: () => of({ roles: [RolesEnum.QUALIFIER] }),
      map: () => of({ creator: false, publisher: false, admin: false, qualifier: true }),
    };
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    directive.visibleByRight = [RolesEnum.QUALIFIER];
    const spy = spyOn(directive['_viewContainer'], 'createEmbeddedView' as any);
    directive.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should clear view for invalidRole', () => {
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    directive.visibleByRight = ["_InvalidRole_"];
    const spy = spyOn(directive['_viewContainer'], 'clear' as any);
    directive.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should create embedded view for role creator', () => {
    // Arrange
    const directive = new VisibleByRightDirective(templateRef as any, viewContainerRef as any, appState as any);
    directive.visibleByRight = [RolesEnum.CREATOR];
    const spy = spyOn(directive['_viewContainer'], 'createEmbeddedView' as any);
    // Act
    directive.ngOnInit();
    // Assert
    expect(spy).toHaveBeenCalled();
  });
});
