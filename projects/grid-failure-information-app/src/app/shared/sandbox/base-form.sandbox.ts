 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BaseSandbox } from './base.sandbox';
import { FormGroupState } from 'ngrx-forms';

export abstract class BaseFormSandbox<T = any> extends BaseSandbox {
  public INITIAL_STATE: FormGroupState<T>;
}
