/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridOptions } from 'ag-grid-community';
import { Subject } from 'rxjs';
import { BaseComponent } from '@grid-failure-information-app/shared/components/base-components/base.component';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';

export abstract class BaseList extends BaseComponent {
  public events$: Subject<any> = new Subject();
  public noRowsTemplate: string;
  public loadingTemplate: string;

  public gridOptions: GridOptions = {
    context: {
      eventSubject: this.events$,
    },
    suppressLoadingOverlay: true,
    localeText: Globals.LOCALE_TEXT,
  };

  protected pageSize: number = 3;

  constructor() {
    super();
    this.noRowsTemplate = `<span>Keine Einträge</span>`;
  }
}
