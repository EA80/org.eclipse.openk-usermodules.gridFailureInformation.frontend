/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component } from '@angular/core';
import { DateCellRendererComponent } from '@grid-failure-information-app/shared/components/cell-renderer/date-cell-renderer/date-cell-renderer.component';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';

@Component({
  selector: 'app-date-time-cell-renderer',
  templateUrl: './date-time-cell-renderer.component.html',
  styleUrls: ['./date-time-cell-renderer.component.scss'],
})
export class DateTimeCellRendererComponent extends DateCellRendererComponent {
  public Globals = Globals;
  public date: string;
  public isDate: boolean = false;
  public constructor() {
    super();
  }
}
