/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit } from '@angular/core';
import { AgRendererComponent } from 'ag-grid-angular';
import { ICellRendererParams, IAfterGuiAttachedParams } from 'ag-grid-community';
import { ModeEnum } from '@grid-failure-information-app/shared/constants/enums';
import { StateEnum } from '@grid-failure-information-app/shared/constants/enums';

@Component({
  selector: 'app-icon-cell-renderer',
  templateUrl: './icon-cell-renderer.component.html',
  styleUrls: ['./icon-cell-renderer.component.scss'],
})
export class IconCellRendererComponent implements AgRendererComponent, OnInit {
  public editIcon = null;
  public deleteIcon = null;
  public readonlyIcon = null;
  public editFlag = null;
  public addIcon = null;
  public removeIcon = null;
  public condensedIcon = null;
  public postcodeIcon = null;
  public params: any;

  private _modeEnum = ModeEnum;

  public agInit(params: ICellRendererParams): void {
    this.params = params;
    this.editFlag = params.data['editable'];
    this._updateIcon();
    if (params.context && params.context.icons) {
      const contextIcons = params.context.icons;
      this.editIcon = !!contextIcons && !!contextIcons.edit;

      this.readonlyIcon = !!contextIcons && !!contextIcons.readonly;
      this.deleteIcon = !!contextIcons && !!contextIcons.delete;
      this.addIcon = !!contextIcons && !!contextIcons.add;
      this.removeIcon = !!contextIcons && !!contextIcons.remove;
      this.condensedIcon = !!contextIcons && !!contextIcons.loadCondensedItems;
      this.postcodeIcon = !!contextIcons && !!contextIcons.postcode;
    }
  }

  public ngOnInit(): void {
    this.params.context.eventSubject.next({ type: 'initialLoad' });
  }

  public refresh(): boolean {
    return false;
  }

  public clicked(eventType: string): void {
    this.params.context.eventSubject.next({ type: eventType, data: this.params.data });
  }

  private _updateIcon(): void {
    this.params.context.eventSubject.subscribe((event: any) => {
      let publicationStatusIsPublishedOrWithdrawn: boolean =
        this.params.data.publicationStatus === StateEnum.PUBLISHED || this.params.data.publicationStatus === StateEnum.WITHDRAWN;
      switch (event.eventType) {
        case this._modeEnum.InitialMode:
          this.editIcon = true && !this.readonlyIcon;
          this.condensedIcon = this.params.data.condensed;
          this.addIcon = false;
          this.removeIcon = false;
          break;
        case this._modeEnum.OverviewTableSelectionMode:
          this.editIcon = false;
          this.addIcon = !this.params.data.condensed && !publicationStatusIsPublishedOrWithdrawn;
          this.condensedIcon = false;
          break;
        case this._modeEnum.CondensationTableSelectionMode:
          this.editIcon = false;
          this.removeIcon = true;
          break;
        case this._modeEnum.CondensationTableNoSelectionMode:
          this.readonlyIcon = this.params.data.statusIntern === StateEnum.COMPLETED || this.params.data.statusIntern === StateEnum.CANCELED;
          this.editIcon = !this.readonlyIcon;
          break;
        case this._modeEnum.oldVersionMode:
          this.deleteIcon = false;
          break;
        case this._modeEnum.currentVersionMode:
          this.deleteIcon = true;
          break;
        default:
          break;
      }
    });
  }
}
