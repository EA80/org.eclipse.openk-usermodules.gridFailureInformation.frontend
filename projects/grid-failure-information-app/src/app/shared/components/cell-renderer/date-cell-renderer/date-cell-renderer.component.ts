/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';

@Component({
  selector: 'app-date-cell-renderer',
  templateUrl: './date-cell-renderer.component.html',
  styleUrls: ['./date-cell-renderer.component.scss'],
})
export class DateCellRendererComponent implements ICellRendererAngularComp {
  public Globals = Globals;
  public date: string;
  public isDate: boolean = false;
  public constructor() {}

  public agInit(params: any) {
    const validDate: number = Date.parse(params.value);
    if (!isNaN(validDate)) {
      this.isDate = true;
    }
    this.date = params.value;
  }

  public refresh(params: any): boolean {
    const validDate: number = Date.parse(params.value);
    if (!isNaN(validDate)) {
      this.isDate = true;
    }
    this.date = params.value;

    return true;
  }
}
