/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, Input, Output, EventEmitter, Injectable } from '@angular/core';
import { NgbTimeStruct, NgbPopoverConfig, NgbDateStruct, NgbDatepickerI18n } from '@ng-bootstrap/ng-bootstrap';
import { DateTimeModel } from '@grid-failure-information-app/shared/models/date-time.model';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { dateTimePickerValueConverter } from '@grid-failure-information-app/shared/utility';

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {
  private readonly _I18N_VALUES = {
    weekdays: ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So'],
    months: ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
  };
  constructor() {
    super();
  }

  public getWeekdayShortName(weekday: number): string {
    return this._I18N_VALUES.weekdays[weekday - 1];
  }
  public getMonthShortName(month: number): string {
    return this._I18N_VALUES.months[month - 1];
  }
  public getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  public getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

@Component({
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],

  providers: [{ provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n }],
})
export class DateTimePickerComponent {
  @Input()
  public inputDatetimeFormat: string = 'd/M/d/yyyy HH:mm:ss';
  @Input()
  public hourStep: number = 1;
  @Input()
  public minuteStep: number = 1;
  @Input()
  public secondStep: number = 30;
  @Input()
  public seconds: boolean = false;
  @Input()
  public isDisabled: boolean = false;

  @Input()
  public isRequired: boolean = false;

  @Input()
  public isValid: boolean = true;

  @Output()
  public dateTimeChange: EventEmitter<any> = new EventEmitter();

  public dateTimeViewValue: string;

  public dateTime: DateTimeModel;

  public Globals = Globals;

  public model: NgbDateStruct;

  public showTimePicker: boolean;

  constructor(private _config: NgbPopoverConfig) {
    this._config.autoClose = 'outside';
    this._config.placement = 'auto';
  }
  public onValueChangeCallBack(value: DateTimeModel): void {}
  public markTouched(): void {}
  public clearDateTime(): void {
    this.dateTimeViewValue = null;
    this.dateTime = null;
    this.onValueChangeCallBack(this.dateTime);
    this.dateTimeChange.emit(this.dateTime);
    this.isValid = !this.isRequired;
  }

  public changeDateTime(ngbDate: NgbTimeStruct | NgbDateStruct): void {
    const dateTime = this.dateTime || new DateTimeModel();
    this.dateTime = new DateTimeModel({
      ...dateTime,
      hour: !!ngbDate['hour'] || ngbDate['hour'] === 0 ? ngbDate['hour'] : dateTime.hour,
      minute: !!ngbDate['minute'] || ngbDate['minute'] === 0 ? ngbDate['minute'] : dateTime.minute,
      second: !!ngbDate['second'] || ngbDate['second'] === 0 ? ngbDate['second'] : dateTime.second,
      year: !!ngbDate['year'] || ngbDate['year'] === 0 ? ngbDate['year'] : dateTime.year,
      month: !!ngbDate['month'] || ngbDate['month'] === 0 ? ngbDate['month'] : dateTime.month,
      day: !!ngbDate['day'] || ngbDate['day'] === 0 ? ngbDate['day'] : dateTime.day,
    });
    this.dateTimeViewValue = dateTimePickerValueConverter.convertStateToViewValue(this.dateTime);

    this.onValueChangeCallBack(this.dateTime);
    this.dateTimeChange.emit(this.dateTimeViewValue);
  }
}
