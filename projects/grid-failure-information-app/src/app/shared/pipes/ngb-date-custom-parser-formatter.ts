/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { Injectable } from '@angular/core';
import { NgbDateTimeStruct } from '@grid-failure-information-app/shared/models/date-time.model';
import { toInteger, isNumber, padNumber } from '@grid-failure-information-app/shared/utility';

@Injectable()
export class NgbDateCustomParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateTimeStruct {
    if (value) {
      const dateParts = value.trim().split('/');
      const day = dateParts[0];
      const month = dateParts[1];
      const year = dateParts[2];
      const hour = dateParts[3];
      const minute = dateParts[4];
      const second = dateParts[5];

      if (dateParts.length === 1 && isNumber(day)) {
        return { day: toInteger(day), month: null, year: null, hour: null, minute: null, second: null };
      } else if (dateParts.length === 2 && isNumber(day) && isNumber(month)) {
        return {
          day: toInteger(day),
          month: toInteger(month),
          year: null,
          hour: null,
          minute: null,
          second: null,
        };
      } else if (dateParts.length === 3 && isNumber(day) && isNumber(month) && isNumber(year)) {
        return {
          day: toInteger(day),
          month: toInteger(month),
          year: toInteger(year),
          hour: null,
          minute: null,
          second: null,
        };
      } else if (dateParts.length === 4 && isNumber(day) && isNumber(month) && isNumber(year) && isNumber(hour)) {
        return {
          day: toInteger(day),
          month: toInteger(month),
          year: toInteger(year),
          hour: toInteger(hour),
          minute: null,
          second: null,
        };
      } else if (dateParts.length === 5 && isNumber(day) && isNumber(month) && isNumber(year) && isNumber(hour) && isNumber(minute)) {
        return {
          day: toInteger(day),
          month: toInteger(month),
          year: toInteger(year),
          hour: toInteger(hour),
          minute: toInteger(minute),
          second: null,
        };
      } else if (dateParts.length === 6 && isNumber(day) && isNumber(month) && isNumber(year) && isNumber(hour) && isNumber(minute) && isNumber(second)) {
        return {
          day: toInteger(day),
          month: toInteger(month),
          year: toInteger(year),
          hour: toInteger(hour),
          minute: toInteger(minute),
          second: toInteger(second),
        };
      }
    }
    return null;
  }

  format(date: NgbDateTimeStruct): string {
    date.hour = 0;
    date.minute = 0;
    return date
      ? `${isNumber(date.day) ? padNumber(date.day) : ''}.${isNumber(date.month) ? padNumber(date.month) : ''}.${date.year} / ${
          isNumber(date.hour) ? padNumber(date.hour) : ''
        }:${isNumber(date.minute) ? padNumber(date.minute) : ''}`
      : '';
  }
}
