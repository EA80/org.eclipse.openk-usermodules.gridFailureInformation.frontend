/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class DistributionGroupTextPlaceholder {
  public branch: string = null;
  public city: string = null;
  public description: string = null;
  public directFailureLink: string = null;
  public district: string = null;
  public expectedReason: string = null;
  public failureBegin: string = null;
  public failureClassification: string = null;
  public failureEndPlanned: string = null;
  public failureEndResupplied: string = null;
  public housenumber: string = null;
  public internalRemark: string = null;
  public postcode: string = null;
  public pressureLevel: string = null;
  public publicationStatus: string = null;
  public radius: number = null;
  public responsibility: string = null;
  public stationDescription: string = null;
  public statusExtern: string = null;
  public statusIntern: string = null;
  public street: string = null;
  public voltageLevel: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
