/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class DistributionGroup {
  public id: string = null;
  public name: string = null;

  public emailSubjectPublish: string = null;
  public distributionTextPublish: string = null;
  public emailSubjectComplete: string = null;
  public distributionTextComplete: string = null;
  public emailSubjectUpdate: string = null;
  public distributionTextUpdate: string = null;

  public emailSubjectView: string = null;
  public distributionTextView: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
