/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { VisibilityConfigurationInterface } from '@grid-failure-information-app/shared/interfaces/visibility-configuration.interface';

export class Settings {
  public overviewMapInitialZoom: number = null;
  public detailMapInitialZoom: number = null;
  public exportChannels: string[] = [];
  public overviewMapInitialLatitude: string = null;
  public overviewMapInitialLongitude: string = null;
  public visibilityConfiguration: VisibilityConfigurationInterface = null;
  public dataExternInitialVisibility: string = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
