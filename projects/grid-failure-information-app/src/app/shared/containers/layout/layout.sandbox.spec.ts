/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { LayoutSandbox } from '@grid-failure-information-app/shared/containers/layout/layout.sandbox';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import * as store from '@grid-failure-information-app/shared/store';
import { of } from 'rxjs';

describe('LayoutSandbox', () => {
  let service: LayoutSandbox = {} as any;
  let translateService: TranslateService;
  let appState: Store<store.State>;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    translateService = { addLangs() {}, setDefaultLang() {}, use() {} } as any;

    service = new LayoutSandbox(appState, translateService);
  });

  it('should create LayoutSandbox service', () => {
    expect(service).toBeTruthy();
    expect(service).toBeDefined();
  });

  it('should call dispatch and translateService.use on selectLanguage', () => {
    const spy1 = spyOn(appState, 'dispatch');
    const spy2 = spyOn(translateService, 'use');
    service.selectLanguage({});
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });
});
