/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as store from '@grid-failure-information-app/shared/store';
import * as settingsActions from '@grid-failure-information-app/shared/store/actions/settings.action';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class LayoutSandbox {
  public selectedLang$ = this.appState$.select(store.getSelectedLanguage);
  public availableLanguages$ = this.appState$.select(store.getAvailableLanguages);

  constructor(protected appState$: Store<store.State>, private translateService: TranslateService) {}

  public selectLanguage(lang: any): void {
    this.appState$.dispatch(settingsActions.setLanguage({ payload: lang.code }));

    this.appState$.dispatch(settingsActions.setCulture({ payload: lang.culture }));
    this.translateService.use(lang.code);
  }
}
