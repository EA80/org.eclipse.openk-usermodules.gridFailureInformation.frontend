/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '../components/components.module';
import { LayoutContainerComponent } from './layout/layout.container';
import { LayoutSandbox } from './layout/layout.sandbox';
import { TranslateModule } from '@ngx-translate/core';
import { CardLayoutComponent } from './card-layout/card-layout.component';

export const CONTAINERS = [LayoutContainerComponent, CardLayoutComponent];

@NgModule({
  imports: [CommonModule, ComponentsModule, TranslateModule],
  declarations: CONTAINERS,
  exports: CONTAINERS,
  providers: [LayoutSandbox],
})
export class ContainersModule {}
