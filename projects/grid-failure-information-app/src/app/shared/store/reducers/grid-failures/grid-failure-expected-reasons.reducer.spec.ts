/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {
  GridFailureExpectedReasonsReducer,
  INITIAL_STATE,
  getData,
  getLoading,
  getLoaded,
  getFailed,
  reducer,
} from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-expected-reasons.reducer';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { FailureExpectedReason } from '@grid-failure-information-app/shared/models';

describe('GridFailureExpectedReasonsReducer', () => {
  it('should return the initial state', () => {
    const action = { type: 'NOOP' } as any;
    const result = reducer(undefined, action);

    expect(result).toBe(INITIAL_STATE);
  });

  it('should trigger loading state', () => {
    const action = gridFailureActions.loadGridFailureExpectedReasons();
    const result = GridFailureExpectedReasonsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      loading: true,
    });
  });

  it('should trigger loaded state', () => {
    const items = { payload: [new FailureExpectedReason()] };
    const action = gridFailureActions.loadGridFailureExpectedReasonsSuccess(items);
    const result = GridFailureExpectedReasonsReducer(INITIAL_STATE, action);

    expect(result.loaded).toBe(true);
  });

  it('should trigger failed state', () => {
    const error = { payload: 'err_msg' };
    const action = gridFailureActions.loadGridFailureExpectedReasonsFail(error);
    const result = GridFailureExpectedReasonsReducer(INITIAL_STATE, action);

    expect(result).toEqual({
      ...INITIAL_STATE,
      failed: true,
    });
  });

  it('getData return state.data', () => {
    const state = { ...INITIAL_STATE };
    const result = getData(state);
    expect(result).toBe(state.data);
  });

  it('getLoading return state.loading', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoading(state);
    expect(result).toBe(state.loading);
  });

  it('getLoaded return state.loaded', () => {
    const state = { ...INITIAL_STATE };
    const result = getLoaded(state);
    expect(result).toBe(state.loaded);
  });

  it('getFailed return state.failed', () => {
    const state = { ...INITIAL_STATE };
    const result = getFailed(state);
    expect(result).toBe(state.failed);
  });
});
