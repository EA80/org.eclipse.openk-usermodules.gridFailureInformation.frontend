/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as formReducer from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-details-form.reducer';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { GridFailure } from '@grid-failure-information-app/shared/models';
import { Action } from '@ngrx/store';
import { FormGroupState } from 'ngrx-forms';

describe('GridFailure Detail Form reducer', () => {
  const { INITIAL_STATE } = formReducer;

  it('should return the default state when no state is given', () => {
    const action: Action = { type: '' };
    const state: FormGroupState<GridFailure> = formReducer.reducer(undefined, action);

    expect(state).toEqual(INITIAL_STATE);
  });

  it('should return the initial state when action is not found', () => {
    const action: Action = { type: '' };
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);

    expect(state).toEqual(INITIAL_STATE);
  });

  it('should return the initial state when no action set', () => {
    const action: Action = null;
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE as FormGroupState<GridFailure>, action);

    expect(state.controls.city.id).toEqual(INITIAL_STATE.controls.city.id);
  });

  it('should return state via getFormState', () => {
    const action: Action = null;
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    expect(formReducer.getFormState(state)).toBe(state);
  });

  it('should set id via loadGridFailureDetailSuccess action', () => {
    const action: Action = { type: gridFailureActions.loadGridFailureDetailSuccess.type };
    action['payload'] = { id: 'x' };
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    expect(state.controls.id).toBeDefined();
  });

  it('should set id via loadGridFailureVersionSuccess action', () => {
    const action: Action = { type: gridFailureActions.loadGridFailureVersionSuccess.type };
    action['payload'] = { id: 'x' };
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    expect(state.controls.id).toBeDefined();
  });

  it('should set FormState in response to gridFailureActions.loadAddressCommunitiesSuccess', () => {
    const action: Action = { type: gridFailureActions.loadAllAddressCommunitiesSuccess.type };
    action['payload'] = [];
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    const formState = formReducer.getFormState(state);
    expect(formState).toEqual(state);
  });

  it('should set FormState in response to gridFailureActions.loadAddressDistrictsSuccess', () => {
    const action: Action = { type: gridFailureActions.loadAddressDistrictsOfCommunitySuccess.type };
    action['payload'] = [];
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    expect(formReducer.getFormState(state)).toEqual(state);
  });

  it('should set FormState in response to gridFailureActions.loadAddressStreetsSuccess', () => {
    const action: Action = { type: gridFailureActions.loadAddressStreetsSuccess.type };
    action['payload'] = [];
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    expect(formReducer.getFormState(state)).toEqual(state);
  });

  it('should set FormState in response to gridFailureActions.loadAddressHouseNumbersSuccess', () => {
    const action: Action = { type: gridFailureActions.loadAddressHouseNumbersSuccess.type };
    action['payload'] = [];
    const state: FormGroupState<GridFailure> = formReducer.reducer(INITIAL_STATE, action);
    expect(formReducer.getFormState(state)).toEqual(state);
  });
});
