/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import * as fromReducer from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-details-form.reducer';
import { DistributionGroup } from '@grid-failure-information-app/shared/models';
import { Action } from '@ngrx/store';
import { FormGroupState } from 'ngrx-forms';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';

describe('DistributionGroup Detail Form reducer', () => {
  const { INITIAL_STATE } = fromReducer;

  it('should return the default state when no state is given', () => {
    const action: Action = { type: '' };
    const state: FormGroupState<DistributionGroup> = fromReducer.reducer(undefined, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return the initial state when action is not found', () => {
    const action: Action = { type: '' };
    const state: FormGroupState<DistributionGroup> = fromReducer.reducer(INITIAL_STATE, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return the initial state when no action set', () => {
    const action: Action = null;
    const state: FormGroupState<DistributionGroup> = fromReducer.reducer(INITIAL_STATE, action);

    expect(state).toBe(INITIAL_STATE);
  });

  it('should return state via getFormState', () => {
    const action: Action = null;
    const state: FormGroupState<DistributionGroup> = fromReducer.reducer(INITIAL_STATE, action);
    expect(fromReducer.getFormState(state)).toBe(state);
  });

  it('should set id', () => {
    const action: Action = { type: distributionGroupActions.loadDistributionGroupsDetailSuccess.type };
    action['payload'] = { id: 'x' };
    const state: FormGroupState<DistributionGroup> = fromReducer.reducer(INITIAL_STATE, action);
    expect(state.controls.id).toBeDefined();
  });
});
