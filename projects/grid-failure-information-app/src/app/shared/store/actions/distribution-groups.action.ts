/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { createAction, props } from '@ngrx/store';
import { DistributionGroup, DistributionGroupMember, DistributionGroupTextPlaceholder } from '@grid-failure-information-app/shared/models';
import { Contact } from '@grid-failure-information-app/shared/models/contact.model';

export const loadDistributionGroups = createAction('[DistributionGroups] Load');
export const loadDistributionGroupsSuccess = createAction('[DistributionGroups] Load Success', props<{ payload: Array<DistributionGroup> }>());
export const loadDistributionGroupsFail = createAction('[DistributionGroups] Load Fail', props<{ payload: string }>());

export const loadDistributionGroupsDetail = createAction('[DistributionGroups Details] Load', props<{ payload: string }>());
export const loadDistributionGroupsDetailSuccess = createAction('[DistributionGroups Details] Load Success', props<{ payload: DistributionGroup }>());
export const loadDistributionGroupsDetailFail = createAction('[DistributionGroups Details] Load Fail', props<{ payload: string }>());

export const saveDistributionGroup = createAction('[DistributionGroup Details] Save', props<{ payload: DistributionGroup }>());
export const saveDistributionGroupSuccess = createAction('[DistributionGroup Details] Save Success', props<{ payload: DistributionGroup }>());
export const saveDistributionGroupFail = createAction('[DistributionGroup Details] Save Fail', props<{ payload: string }>());

export const deleteDistributionGroup = createAction('[DistributionGroup Details] Delete', props<{ payload: string }>());
export const deleteDistributionGroupSuccess = createAction('[DistributionGroup Details] Delete Success');
export const deleteDistributionGroupFail = createAction('[DistributionGroup Details] Delete Fail', props<{ payload: string }>());

export const loadDistributionGroupMembers = createAction('[DistributionGroupMembers] Load', props<{ payload: string }>());
export const loadDistributionGroupMembersSuccess = createAction(
  '[DistributionGroupMembers] Load Success',
  props<{ payload: Array<DistributionGroupMember> }>()
);

export const resetDistributionGroupMembers = createAction('[DistributionGroupMembers] Reset Success');
export const loadDistributionGroupMembersFail = createAction('[DistributionGroupMembers] Load Fail', props<{ payload: string }>());

export const deleteDistributionGroupMember = createAction('[DistributionGroupMember] Delete', props<{ groupId: string; memberId: string }>());
export const deleteDistributionGroupMemberSuccess = createAction('[DistributionGroupMember] Delete Success');
export const deleteDistributionGroupMemberFail = createAction('[DistributionGroupMember] Delete Fail', props<{ payload: string }>());

export const loadContacts = createAction('[Contacts] Load', props<{ searchText: string }>());
export const loadContactsSuccess = createAction('[Contacts] Load Success', props<{ payload: Array<Contact> }>());
export const loadContactsFail = createAction('[Contacts] Load Fail', props<{ payload: string }>());

export const createDistributionGroupMember = createAction('[DistributionGroupMember] Create', props<{ groupId: string; newMember: DistributionGroupMember }>());
export const createDistributionGroupMemberSuccess = createAction('[DistributionGroupMember] Create Success', props<{ payload: DistributionGroupMember }>());
export const createDistributionGroupMemberFail = createAction('[DistributionGroupMember] Create Fail', props<{ payload: string }>());

export const updateDistributionGroupMember = createAction(
  '[DistributionGroupMember] Update',
  props<{ groupId: string; memberId: string; member: DistributionGroupMember }>()
);
export const updateDistributionGroupMemberSuccess = createAction('[DistributionGroupMember] Update Success', props<{ payload: DistributionGroupMember }>());
export const updateDistributionGroupMemberFail = createAction('[DistributionGroupMember] Update Fail', props<{ payload: string }>());

export const loadDistributionGroupTextPlaceholders = createAction('[DistributionGroupTextPlaceholder] Load');
export const loadDistributionGroupTextPlaceholdersSuccess = createAction(
  '[DistributionGroupTextPlaceholder] Load Success',
  props<{ payload: DistributionGroupTextPlaceholder }>()
);
export const loadDistributionGroupTextPlaceholdersFail = createAction('[DistributionGroupTextPlaceholder] Load Fail', props<{ payload: string }>());

export const changeDistributionMailTemplate = createAction('[DistributionGroupMailTemplate] Change', props<{ oldTemplate: string; newTemplate: string }>());

export const exportContacts = createAction('[DistributionGroupMember] Export', props<{ groupId: string }>());
export const exportContactsSuccess = createAction('[DistributionGroupMember] Export Success', props<{ payload: Object }>());
export const exportContactsFail = createAction('[DistributionGroupMember] Export Fail', props<{ payload: string }>());

export const clearAssignmentInput = createAction('[DistributionGrouAssignmentInput] Clear');
