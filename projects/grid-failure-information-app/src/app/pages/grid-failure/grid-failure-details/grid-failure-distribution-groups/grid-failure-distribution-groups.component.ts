/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { GRID_FAILURE_DISTRIBUTION_GROUPS_COLDEF } from '@grid-failure-information-app/app/pages/grid-failure/grid-failure-details/grid-failure-distribution-groups/grid-failure-distribution-groups-column-definition.ts';
import { GridFailureDetailsSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.sandbox';
import { BaseList } from '@grid-failure-information-app/shared/components/base-components/base.list';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { DistributionGroup } from '@grid-failure-information-app/shared/models/distribution-group.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-grid-failure-distribution-groups',
  templateUrl: './grid-failure-distribution-groups.component.html',
  styleUrls: ['./grid-failure-distribution-groups.component.scss'],
})
export class GridFailureDistributionGroupsComponent extends BaseList implements OnInit, OnDestroy {
  @Output()
  public changed = new EventEmitter();
  public columnDefinition: any = GRID_FAILURE_DISTRIBUTION_GROUPS_COLDEF;
  public frameworkComponents: { setFilterComponent: any };
  private _selectedGroup: DistributionGroup;
  public get selectedGroup(): DistributionGroup {
    return this._selectedGroup;
  }
  public set selectedGroup(value: DistributionGroup) {
    this._selectedGroup = value;
  }
  private _subscription: Subscription;

  constructor(public detailsSandbox: GridFailureDetailsSandbox) {
    super();
    this.frameworkComponents = { setFilterComponent: SetFilterComponent };
  }

  ngOnInit() {
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { delete: true },
    };
    this._subscription = this.gridOptions.context.eventSubject.subscribe(event => {
      if (event.type === 'delete') {
        this.detailsSandbox.deleteDistributionGroupAssignment(event.data.id);
        this.changed.emit();
      }
    });
  }

  public clearSelectedGroup(): void {
    this.selectedGroup = null;
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }
}
