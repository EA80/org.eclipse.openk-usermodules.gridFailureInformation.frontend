/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { GridFailureDetailsSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.sandbox';
import { ModeEnum, RolesEnum, StateEnum, VoltageLevelEnum, FaultLocationAreaEnum } from '@grid-failure-information-app/shared/constants/enums';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { GridFailure } from '@grid-failure-information-app/shared/models/grid-failure.model';
import * as store from '@grid-failure-information-app/shared/store';
import { Observable, Subscription } from 'rxjs';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';
import { BaseList } from '@grid-failure-information-app/shared/components/base-components/base.list';
import { STATION_COLDEF } from './station-list-column-definition';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { take, skipWhile, takeUntil, map } from 'rxjs/operators';
import { determineDetailFieldVisibility } from '@grid-failure-information-app/shared/utility';
import { ofType } from '@ngrx/effects';
import { ActionsSubject, Store } from '@ngrx/store';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';

@Component({
  selector: 'app-grid-failure-details',
  templateUrl: './grid-failure-details.component.html',
  styleUrls: ['./grid-failure-details.component.scss'],
})
export class GridFailureDetailsComponent extends BaseList implements OnInit, OnDestroy {
  @ViewChild('searchInput', { static: false }) searchInput: ElementRef;

  public Globals = Globals;
  public RolesEnum = RolesEnum;
  public StateEnum = StateEnum;
  public VoltageLevelEnum = VoltageLevelEnum;
  public gridFailureVersions$: Observable<GridFailure[]> = this.appState$.select(store.getGridFailureVersionsData);
  public currentAddress: string = '';

  public getGgridFailureVersion = (versionNumber: string) =>
    this.gridFailureVersions$.pipe(map((versions: GridFailure[]) => versions.find((version: GridFailure) => version.versionNumber === +versionNumber)));
  private readonly _initialLocationView = Globals.FAILURE_LOCATION_NS;
  public failureLocationView: string = this._initialLocationView;
  public mapOptions: MapOptions = new MapOptions();
  public get mapInteractionMode(): boolean {
    return (
      this.failureLocationView === Globals.FAILURE_LOCATION_MAP &&
      !this.gridFailureDetailsSandbox.oldVersion &&
      this.gridFailureDetailsSandbox.currentFormState.isEnabled
    );
  }
  public stationsColumnDefinition: any = STATION_COLDEF;
  public frameworkComponents: any;

  private _subscription: Subscription;
  private _modeEnum = ModeEnum;

  constructor(public gridFailureDetailsSandbox: GridFailureDetailsSandbox, protected appState$: Store<store.State>, protected actionsSubject: ActionsSubject) {
    super();
    this.frameworkComponents = { setFilterComponent: SetFilterComponent };
  }

  ngOnInit() {
    this.gridFailureDetailsSandbox.init();
    this.gridFailureDetailsSandbox.registerEvents();
    this._initialFailureLocationState();

    this._waitForPreConfig().then(() => {
      this.gridFailureDetailsSandbox.gridFailureDetailsFormState$.subscribe(formState => {
        this.gridOptions.context = {
          ...this.gridOptions.context,
          icons: { delete: !this.gridFailureDetailsSandbox.oldVersion && formState.isEnabled },
        };
      });

      this._subscription = this.gridOptions.context.eventSubject.subscribe(event => {
        if (event.type === 'delete') {
          this.gridFailureDetailsSandbox.deleteGridFailureStation(event.data.id);
        }
      });
      this.gridFailureDetailsSandbox.setFormStatePristine();

      this.gridFailureDetailsSandbox.gridFailureDetailsFormState$
        .pipe(
          skipWhile(item => !item.value.postcode && !item.value.city && !item.value.district && !item.value.street && !item.value.housenumber),
          take(1)
        )
        .subscribe(girdFailure => {
          if (girdFailure.value.postcode) {
            this.currentAddress += `${girdFailure.value.postcode} `;
          }
          if (girdFailure.value.city) {
            this.currentAddress += `${girdFailure.value.city} `;
          }
          if (girdFailure.value.district) {
            this.currentAddress += `(${girdFailure.value.district})`;
          }
          if (girdFailure.value.street) {
            this.currentAddress += `, ${girdFailure.value.street} `;
          }
          if (girdFailure.value.housenumber) {
            this.currentAddress += `${girdFailure.value.housenumber}`;
          }
        });
    });
  }
  public setBranchValue(branchId: string) {
    const branch = this.gridFailureDetailsSandbox.branches.find(b => b.id === branchId);
    this.gridFailureDetailsSandbox.setBranchState(branch);
  }
  public setLocation() {
    this.actionsSubject
      .pipe(
        ofType(...[gridFailureActions.loadGridFailureDetailSuccess.type, gridFailureActions.loadGridFailureVersionSuccess.type]),
        map(action => action['payload']['faultLocationArea']),
        take(1)
      )
      .subscribe(faultLocationArea => {
        switch (faultLocationArea) {
          case FaultLocationAreaEnum.Address:
            this.failureLocationView = Globals.FAILURE_LOCATION_NS;
            break;
          case FaultLocationAreaEnum.Station:
            this.failureLocationView = Globals.FAILURE_LOCATION_MS;
            break;
          case FaultLocationAreaEnum.Map:
            this.failureLocationView = Globals.FAILURE_LOCATION_MAP;
            break;

          default:
            this.failureLocationView = Globals.FAILURE_LOCATION_NS;
            break;
        }
        this.gridFailureDetailsSandbox.currentFormState.isEnabled && this.gridFailureDetailsSandbox.setViewStateForReqProps(this.failureLocationView);
      });
  }
  public resetCoords(value: string): void {
    if (!value) {
      this.gridFailureDetailsSandbox.resetCoords();
    }
  }

  public resetSelectedStation(value: string): void {
    if (!value) {
      this.gridFailureDetailsSandbox.setSelectedStation(null);
    }
  }

  public resizeSetMap() {
    this.mapOptions.forceResize$.next(true);
  }

  public setViewStateForReqProps() {
    this.gridFailureDetailsSandbox.setViewStateForReqProps(this.failureLocationView);
  }

  public clearSearchInput() {
    this.searchInput.nativeElement.value = '';
  }

  public setNewGridOptions(currentV: string) {
    this.gridOptions.context = {
      ...this.gridOptions.context,
      icons: { delete: this.gridFailureDetailsSandbox.maxVersionNumber === +currentV },
    };
    this.gridOptions = { ...this.gridOptions };
  }

  public changeMode() {
    this.gridFailureDetailsSandbox.gridFailureStations$.pipe(take(1), takeUntil(this._endSubscriptions$)).subscribe(() => {
      this.gridFailureDetailsSandbox.oldVersion
        ? this.events$.next({ eventType: this._modeEnum.oldVersionMode })
        : this.events$.next({ eventType: this._modeEnum.currentVersionMode });
    });
    this._initialFailureLocationState();
  }

  public isLocationButtonForStationVisible(branch: string, voltageLevel: string = '') {
    if (branch === Globals.BUSINESS_RULE_FIELDS.branch.telecommunication) {
      return true;
    } else if (branch === Globals.BUSINESS_RULE_FIELDS.branch.power && voltageLevel === Globals.FAILURE_LOCATION_MS) {
      return true;
    } else if (
      branch === Globals.BUSINESS_RULE_FIELDS.branch.water ||
      branch === Globals.BUSINESS_RULE_FIELDS.branch.gas ||
      branch === Globals.BUSINESS_RULE_FIELDS.branch.secondaryTechnology ||
      branch === Globals.BUSINESS_RULE_FIELDS.branch.districtHeating
    ) {
      return false;
    }
  }

  public determineDetailFieldVisibility(field: string): boolean {
    return determineDetailFieldVisibility(this.mapOptions.visibilityConfiguration, 'fieldVisibility', field);
  }

  private _initialFailureLocationState() {
    this.failureLocationView = Globals.FAILURE_LOCATION_NS;
    this.setLocation();
  }
  ngOnDestroy() {
    this.gridFailureDetailsSandbox.gridFailureStations = [];
    this.gridFailureDetailsSandbox.endSubscriptions();
    this._subscription.unsubscribe();
  }

  setLocationRegionToAddress() {
    if (this.isLocationRegionActiveForAddress()) {
      this.failureLocationView = Globals.FAILURE_LOCATION_NS;
      this.setLocationRegion();
    }
  }
  isLocationRegionActiveForAddress(): boolean {
    const ret = this.failureLocationView && this.failureLocationView !== Globals.FAILURE_LOCATION_NS;
    this.gridFailureDetailsSandbox.setFaultLocationArea(FaultLocationAreaEnum.Address);
    return ret;
  }
  setLocationRegionToStation() {
    if (this.isLocationRegionActiveForStation()) {
      this.failureLocationView = Globals.FAILURE_LOCATION_MS;
      this.gridFailureDetailsSandbox.setFaultLocationArea(FaultLocationAreaEnum.Station);
      this.setLocationRegion();
    }
  }
  isLocationRegionActiveForStation(): boolean {
    const ret = this.failureLocationView && this.failureLocationView !== Globals.FAILURE_LOCATION_MS;
    return ret;
  }
  setLocationRegionToMap() {
    if (this.isLocationRegionActiveForMap()) {
      this.failureLocationView = Globals.FAILURE_LOCATION_MAP;
      this.gridFailureDetailsSandbox.setFaultLocationArea(FaultLocationAreaEnum.Map);
      this.setLocationRegion();
    }
  }
  isLocationRegionActiveForMap(): boolean {
    const ret = this.failureLocationView && this.failureLocationView !== Globals.FAILURE_LOCATION_MAP;
    return ret;
  }
  private setLocationRegion() {
    this.setViewStateForReqProps();
    this.gridFailureDetailsSandbox.resetFailureLocationValues();
    this.resizeSetMap();
    this.gridFailureDetailsSandbox.setFormStateDirty();
  }
  private async _waitForPreConfig(): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this.appState$.select(store.getPreConfiguration).subscribe(preConfig => {
        if (preConfig) {
          resolve((this.mapOptions = new MapOptions(preConfig)));
        }
      });
    });
    return promise;
  }
}
