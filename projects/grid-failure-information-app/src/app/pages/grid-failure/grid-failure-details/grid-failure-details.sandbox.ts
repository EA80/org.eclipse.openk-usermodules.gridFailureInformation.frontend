/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { ConfigService } from '@grid-failure-information-app/app/app-config.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SafetyQueryDialogComponent } from '@grid-failure-information-app/shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import * as enums from '@grid-failure-information-app/shared/constants/enums';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import * as models from '@grid-failure-information-app/shared/models';
import { BaseFormSandbox } from '@grid-failure-information-app/shared/sandbox/base-form.sandbox';
import * as store from '@grid-failure-information-app/shared/store';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import {
  DEPENDENT_FIELD_KEY,
  FORM_ID,
  INITIAL_STATE,
  NO_BRANCH_ID_KEY,
  GAS_BRANCH_ID_KEY,
  POWER_BRANCH_ID_KEY,
} from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-details-form.reducer';
import { navigateHome, stationToStationDescriptionConverter, sortAlphaNum } from '@grid-failure-information-app/shared/utility';
import { unboxProperties } from '@grid-failure-information-app/shared/utility/form-utils';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ofType } from '@ngrx/effects';
import { ActionsSubject, Store } from '@ngrx/store';
import {
  DisableAction,
  EnableAction,
  FormGroupState,
  NgrxValueConverter,
  NgrxValueConverters,
  ResetAction,
  SetUserDefinedPropertyAction,
  SetValueAction,
  box,
  MarkAsDirtyAction,
  unbox,
  MarkAsPristineAction,
} from 'ngrx-forms';
import { combineLatest, Observable, of, ReplaySubject, BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, skipWhile, take, takeUntil, tap } from 'rxjs/operators';
import { StateEnum } from '@grid-failure-information-app/shared/constants/enums';

@Injectable()
export class GridFailureDetailsSandbox extends BaseFormSandbox<models.GridFailure> {
  public Globals = Globals;
  public StateEnum = enums.StateEnum;
  public PublicationStatusEnum = enums.PublicationStatusEnum;
  public gridFailureDetailsFormState$: Observable<FormGroupState<models.GridFailure>> = this.appState$.select(store.getGridFailuresDetails);
  public currentFormState: FormGroupState<models.GridFailure>;
  public gridFailureClassifications$: Observable<models.FailureClassification[]> = this.appState$.select(store.getGridFailureClassificationsData);
  public gridFailureBranches$: Observable<models.FailureBranch[]> = this.appState$.select(store.getGridFailureBranchesData);
  public stations$: Observable<models.FailureStation[]> = this.appState$.select(store.getStationsData);
  public gridFailureStations$: Observable<models.FailureStation[]> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadGridFailureStationsSuccess.type),
    map(action => action['payload'])
  );
  public histGridFailureStations$: Observable<models.FailureStation[]> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadHistGridFailureStationsSuccess.type),
    map(action => action['payload'])
  );
  public gridFailurePolygon$: Observable<Array<[number, number]>> = this.appState$.select(store.getGridFailurePolygonData);
  public gridFailureActions = gridFailureActions;
  public gridFailureStates$: Observable<models.FailureState[]> = this.appState$.select(store.getGridFailureStatesData);
  public gridFailureRadii$: Observable<models.FailureRadius[]> = this.appState$.select(store.getGridFailureRadiiData);
  public datePickerResetEnum = enums.DatePickerResetEnum;
  public voltageLevelEnum = enums.VoltageLevelEnum;
  public gridFailureExpectedReasons$: Observable<models.FailureExpectedReason[]> = this.appState$.select(store.getGridFailureExpectedReasonsData);
  public pressureLevelEnum = enums.PressureLevelEnum;
  public publicationStatusEnum = enums.PublicationStatusEnum;
  public maxVersionNumber: number;
  public currentGridFailureDetailsCoords: models.FailureCoordsInformation = new models.FailureCoordsInformation();
  public noBranchId: string;
  public powerBranchId: string;
  public isFieldRequiredDependingOnBranchId: boolean = false;
  private _testCounter = 0;
  public addressCommunities$: Observable<Array<string>> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadAllAddressCommunitiesSuccess),
    map(action => {
      const stringArray = action.payload;
      return stringArray.sort();
    }),
    takeUntil(this._endSubscriptions$)
  );
  public addressDistricts$: Observable<Array<string>> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadAddressDistrictsOfCommunitySuccess),
    map(action => {
      const stringArray = action.payload;
      return stringArray.sort();
    }),
    takeUntil(this._endSubscriptions$)
  );
  public addressStreets$: Observable<Array<string>> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadAddressStreetsSuccess),
    map(action => {
      const stringArray = action.payload;
      return stringArray.sort();
    }),
    takeUntil(this._endSubscriptions$)
  );

  private _addressHouseNumbers$: BehaviorSubject<Array<models.FailureHousenumber>> = new BehaviorSubject<Array<models.FailureHousenumber>>([]);
  public addressHouseNumbers$: Observable<Array<string>> = this._addressHouseNumbers$.pipe(
    map((hnrs: models.FailureHousenumber[]) => hnrs.map(hnr => hnr.housenumber))
  );

  public addressPostCodes$: Observable<Array<string>> = this.actionsSubject.pipe(
    ofType(gridFailureActions.loadAddressPostalcodesSuccess),

    map(action => {
      const stringArray = action.payload;
      return stringArray.sort(sortAlphaNum);
    }),
    takeUntil(this._endSubscriptions$)
  );
  public showQualifyButton: boolean = false;
  public showStornoButton: boolean = false;
  public showCreatedButton: boolean = false;
  public showUpdateButton: boolean = false;
  public showPublishButton: boolean = false;
  public showCompleteButton: boolean = false;
  public showDeleteButton: boolean = false;
  public gridFailureAddress: models.FailureAddress;
  public gridFailureStations: models.FailureStation[] = [];

  public gridFailureDistributionGroups$: Observable<models.DistributionGroup[]> = this.appState$.select(store.getGridFailureDistributionGroupsData);
  public gridFailureDistributionGroupsLoading$: Observable<boolean> = this.appState$.select(store.getGridFailureDistributionGroupsLoading);
  public distributionGroups$: Observable<models.DistributionGroup[]> = this.appState$.select(store.getDistributionGroupsData);
  public exportChannels$: Observable<string[]> = this.appState$.select(store.getExportChannels);
  public gridFailurePublicationChannels$: Observable<models.PublicationChannel[]> = this.appState$.select(store.getGridFailurePublicationChannelsData);
  public disablePublishButton: boolean;
  public oldVersion = false;
  public selectedPublicationChannels: Array<string>;
  public branches: models.FailureBranch[];
  public branch: models.FailureBranch;

  private _gridFailureId: string;
  private _selectedStation: models.FailureStation;
  private _stations: Array<models.FailureStation> = new Array<models.FailureStation>();
  private _assignedDistributionGroups: Array<models.DistributionGroup>;

  constructor(
    protected appState$: Store<store.State>,
    protected actionsSubject: ActionsSubject,
    private _router: Router,
    private _utilService: UtilService,
    private _modalService: NgbModal,
    private _configService: ConfigService
  ) {
    super(appState$);
  }

  public init() {
    this._setBranchIds();

    this.gridFailureStations$.pipe(take(1), takeUntil(this._endSubscriptions$)).subscribe(stations => {
      this.gridFailureStations = stations;
      // set coords if first element was inserted
      !!stations.length && this.latLonMapping({ longitude: stations[0].longitude, latitude: stations[0].latitude }, true);
      if (this.gridFailureStations && !!this.gridFailureStations.length) {
        this._calculatePolygon(this.gridFailureStations.map(station => station.id));
      }
    });
  }

  public loadGridFailure(gridFailureId: string): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureDetail({ payload: gridFailureId }));
  }

  public loadGridFailureBranches(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureBranches());
  }

  public loadGridFailureClassifications(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureClassifications());
  }

  public loadGridFailureStates(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureStates());
  }

  public loadGridFailureRadii(): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureRadii());
  }

  public loadGridFailureCommunities(): void {
    this.appState$.dispatch(gridFailureActions.loadAllAddressCommunities({ branch: null }));
  }

  public loadGridFailureDistricts(community: string, branch: string): void {
    this.appState$.dispatch(gridFailureActions.loadAddressDistrictsOfCommunity({ branch: branch, community: community }));
  }

  public dateValueConverter = NgrxValueConverters.objectToJSON;
  private _registerLoadGridFailureExpectedReasons(): void {
    combineLatest(this.gridFailureBranches$, this.gridFailureDetailsFormState$)
      .pipe(
        skipWhile(([branches, formState]) => !this.currentFormState || branches.length === 0),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe(([branches, formState]) => {
        let currentBranch = branches.find(branch => !!formState && branch.id === formState.value.branchId);
        this.currentFormState.value.branchId !== formState.value.branchId && !!currentBranch && this.loadGridFailureExpectedReasons(currentBranch);
      });

    combineLatest(this.gridFailureBranches$, this.gridFailureDetailsFormState$)
      .pipe(
        skipWhile(([branches, formState]) => !formState || branches.length === 0),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe(([branches, formState]) => {
        let currentBranch = branches.find(branch => branch.id === formState.value.branchId);
        !!currentBranch && this.loadGridFailureExpectedReasons(currentBranch);
      });
  }

  public loadGridFailureExpectedReasons(branch: models.FailureBranch): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureExpectedReasons({ payload: !!branch ? branch.name : '' }));
  }

  public loadStations(): void {
    this.actionsSubject
      .pipe(
        ofType(gridFailureActions.loadStationsSuccess),
        take(1),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((stations: Array<models.FailureStation>) => {
        this._stations = stations;
      });
    this.appState$.dispatch(gridFailureActions.loadStations());
  }

  public loadGridFailureVersions(gridFailureId: string): void {
    this.actionsSubject
      .pipe(
        ofType(gridFailureActions.loadGridFailureVersionsSuccess),
        take(1),
        takeUntil(this._endSubscriptions$),
        map(action => {
          const gridFailures: models.GridFailure[] = action['payload'];
          return !!gridFailures && Math.max(...gridFailures.map(o => o.versionNumber));
        })
      )
      .subscribe(versionNumber => {
        this.maxVersionNumber = versionNumber;
      });
    this.appState$.dispatch(gridFailureActions.loadGridFailureVersions({ payload: gridFailureId }));
  }

  public loadDistributionGroups(): void {
    this.actionsSubject
      .pipe(
        ofType(gridFailureActions.loadGridFailureDistributionGroupsSuccess),
        take(1),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((assignedGroups: Array<models.DistributionGroup>) => {
        this._assignedDistributionGroups = assignedGroups;
      });
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroups());
  }

  public loadGridFailureVersion(versionNumber: string): void {
    if (!!versionNumber && +versionNumber < this.maxVersionNumber) {
      this.appState$.dispatch(gridFailureActions.loadGridFailureVersion({ gridFailureId: this._gridFailureId, versionNumber: +versionNumber }));
      this.appState$.dispatch(gridFailureActions.loadHistGridFailureStations({ failureId: this._gridFailureId, versionNumber: versionNumber }));
      !this.currentFormState.isDisabled && this.appState$.dispatch(new DisableAction(FORM_ID));
      this.oldVersion = true;
    } else {
      this.appState$.dispatch(gridFailureActions.loadGridFailureDetail({ payload: this._gridFailureId }));
      this.appState$.dispatch(gridFailureActions.loadGridFailureStations({ payload: this._gridFailureId }));
      this.currentFormState.isDisabled && this.appState$.dispatch(new EnableAction(FORM_ID));
      this.oldVersion = false;
    }

    this._setGridFailureStations(versionNumber);
  }

  public loadGridFailureDistributionGroups(gridFailureId: string): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailureDistributionGroups({ payload: gridFailureId }));
  }

  public loadGridFailurePublicationChannels(gridFailureId: string): void {
    this.appState$.dispatch(gridFailureActions.loadGridFailurePublicationChannels({ payload: gridFailureId }));
  }

  public setGridFailureId(gridFailureId: string) {
    this._gridFailureId = gridFailureId;
  }

  public setState(newState: string): void {
    this.gridFailureStates$
      .pipe(
        map(states => states.filter(item => item.status === newState)),
        take(1),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe(states => {
        if (states && states.length > 0) {
          this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.statusInternId.id, states[0].id));
          this.saveGridFailure();
        }
      });
  }

  public setFaultLocationArea(area: string) {
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.faultLocationArea.id, area));
  }

  public setPublishedState(): void {
    if (this.selectedPublicationChannels.length === 0) {
      this._utilService.displayNotification('PublicationRequiresAtLeastOneChannel', 'alert');
    } else {
      this.saveGridFailure(true);
    }
  }

  public deleteGridFailure(): void {
    const modalRef: NgbModalRef = this._modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.actionsSubject.pipe(ofType(gridFailureActions.deleteGridFailureSuccess), take(1)).subscribe(() => this._clear());
        this.appState$.dispatch(gridFailureActions.deleteGridFailure({ gridFailureId: this._gridFailureId }));
      },
      () => {
        modalRef.close();
      }
    );
  }

  public cancel(): void {
    if (!this.currentFormState.isPristine && !this.oldVersion) {
      const modalRef = this._modalService.open(SafetyQueryDialogComponent);
      modalRef.componentInstance.title = 'ConfirmDialog.Action.edit';
      modalRef.componentInstance.body = 'ConfirmDialog.Content';
      modalRef.result.then(
        () => {
          this._clear();
        },
        () => {}
      );
    } else {
      this._clear();
    }
  }

  public setBranchState(branch: models.FailureBranch): void {
    this.appState$.dispatch(new SetValueAction(this.currentFormState.controls.__formBranch.id, branch));
  }

  public saveGridFailure(saveForPublish: boolean = false): void {
    if (this.currentFormState.isValid) {
      const stationIds = this.gridFailureStations.map(station => station.id);
      let savePayload = { gridFailure: unboxProperties<models.GridFailure>(this.currentFormState.value), saveForPublish: saveForPublish };
      savePayload.gridFailure.stationIds = stationIds;

      this.actionsSubject
        .pipe(
          ofType(gridFailureActions.saveGridFailureSuccess),
          map(action => action.payload),
          take(1),
          takeUntil(this._endSubscriptions$)
        )
        .subscribe((payload: any) => {
          this.setFormStatePristine();
          if (payload && payload.id) {
            this._router.navigateByUrl('/grid-failures/' + payload.id).then(() => {
              this._reloadDetail();
            });
          } else {
            this._reloadDetail();
          }
        });

      this.appState$.dispatch(
        gridFailureActions.saveGridFailure({
          payload: savePayload,
        })
      );
    } else {
      // Checks if only coordinates or station or address invalid
      const latControl = this.currentFormState.controls.latitude;
      const longControl = this.currentFormState.controls.longitude;
      const cityControl = this.currentFormState.controls.city;
      const districtControl = this.currentFormState.controls.district;
      const postcodeControl = this.currentFormState.controls.postcode;
      const streetControl = this.currentFormState.controls.street;
      const housenumberControl = this.currentFormState.controls.housenumber;
      const radiusControl = this.currentFormState.controls.radiusId;
      const allButGridFailureLocationAreValid = Object.keys(this.currentFormState.controls || {}).every((controlKey: string) => {
        return (
          controlKey === 'id' ||
          this.currentFormState.controls[controlKey].id === latControl.id ||
          this.currentFormState.controls[controlKey].id === longControl.id ||
          this.currentFormState.controls[controlKey].id === cityControl.id ||
          this.currentFormState.controls[controlKey].id === districtControl.id ||
          this.currentFormState.controls[controlKey].id === postcodeControl.id ||
          this.currentFormState.controls[controlKey].id === streetControl.id ||
          this.currentFormState.controls[controlKey].id === housenumberControl.id ||
          this.currentFormState.controls[controlKey].id === radiusControl.id ||
          this.currentFormState.controls[controlKey].isValid
        );
      });
      if (
        allButGridFailureLocationAreValid &&
        (!(latControl.isValid && longControl.isValid) ||
          !cityControl.isValid ||
          !districtControl.isValid ||
          !postcodeControl.isValid ||
          !streetControl.isValid ||
          !housenumberControl.isValid)
      ) {
        this._utilService.displayNotification('MandatoryMapFieldError', 'alert');
      } else if (allButGridFailureLocationAreValid && latControl.isValid && longControl.isValid && !radiusControl.isValid) {
        this._utilService.displayNotification('MandatoryRadiusFieldError', 'alert');
      } else {
        this._utilService.displayNotification('MandatoryFieldError', 'alert');
      }
    }
  }

  private _setLatLong(hsnr: string): void {
    if (!hsnr) {
      return;
    }
    this._addressHouseNumbers$.pipe(take(1)).subscribe((addresses: Array<models.FailureHousenumber>) => {
      let addressHouseNumber: models.FailureHousenumber;
      if (!!addresses && addresses.length > 0) {
        addressHouseNumber = addresses.find((housenumber: models.FailureHousenumber) => {
          return housenumber.housenumber === hsnr;
        });
      }
      if (!!addressHouseNumber) {
        this.appState$.dispatch(gridFailureActions.loadGridFailureAddress({ payload: addressHouseNumber.uuid }));
      }
    });
  }

  public registerEvents(): void {
    this._registerLoadGridFailureExpectedReasons();
    this.actionsSubject
      .pipe(
        ofType(gridFailureActions.loadAddressHouseNumbersSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe(houseNumbers => {
        if (houseNumbers.length > 0) {
          houseNumbers.sort(sortAlphaNum);
          this._addressHouseNumbers$.next(houseNumbers);
          this._setLatLong(this.currentFormState.value.housenumber);
        }
      });
    this.actionsSubject
      .pipe(
        ofType(gridFailureActions.loadGridFailureAddressSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((address: models.FailureAddress) => {
        this.gridFailureAddress = address;
        const event = { longitude: address.longitude, latitude: address.latitude };
        this.latLonMapping(event, false);
      });

    this.gridFailureDetailsFormState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<models.GridFailure>) => {
      if (!formState.value || !formState.controls) return;

      this.currentFormState = formState;
      this._showButtonsByState(formState.value.statusIntern);
      this._setDynamicRequired();
      this._handleUserDefinedProperties(formState);
    });

    this.gridFailurePolygon$.pipe(takeUntil(this._endSubscriptions$)).subscribe((polygon: Array<[number, number]>) => {
      this.currentFormState.value.addressPolygonPoints = !!polygon ? box(polygon) : null;
      this.currentGridFailureDetailsCoords = new models.FailureCoordsInformation(this.currentFormState.value);
    });

    this.gridFailurePublicationChannels$.pipe(takeUntil(this._endSubscriptions$)).subscribe((channels: Array<models.PublicationChannel>) => {
      this.selectedPublicationChannels = channels.map(s => s.publicationChannel);
      this.disablePublishButton = channels.length > 0 && !channels.find(channel => !channel.published);
    });

    combineLatest([this.gridFailureDetailsFormState$, this.gridFailureRadii$])
      .pipe(takeUntil(this._endSubscriptions$))
      .subscribe(([gridFailureDetails, radiusIds]) => {
        let failureRadius: models.FailureRadius;
        if (!!gridFailureDetails.value.radiusId) {
          if (radiusIds.length !== 0) {
            failureRadius = radiusIds.find(item => item.id === gridFailureDetails.value.radiusId);
            gridFailureDetails.value.radius = failureRadius.radius;
          }
        }

        if (
          this.currentGridFailureDetailsCoords.latitude !== gridFailureDetails.value.latitude ||
          this.currentGridFailureDetailsCoords.longitude !== gridFailureDetails.value.longitude ||
          this.currentGridFailureDetailsCoords.radius !== gridFailureDetails.value.radius
        ) {
          this.currentGridFailureDetailsCoords = new models.FailureCoordsInformation(gridFailureDetails.value);
        }
      });
  }

  public searchForStation = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => (term.length < 2 ? [] : this._stations.filter(s => s.failureStationSearchString.toLowerCase().indexOf(term.toLowerCase()) > -1)))
    );

  public formatter = (s: models.FailureStation | string) => {
    if (s instanceof models.FailureStation) return s.failureStationSearchString;
    else return s;
  };

  public stationValueConverter: NgrxValueConverter<any | null, string | null> = stationToStationDescriptionConverter;

  private _calculatePolygon(stationIds: string[]) {
    this.appState$.dispatch(gridFailureActions.loadGridFailurePolygon({ payload: stationIds }));
  }

  public latLonMapping(data: { longitude: number; latitude: number }, isInitialLoading: boolean): void {
    const isSelectedMapLocationDifferentToOldMapLocation =
      !this.currentFormState.value.latitude ||
      data.latitude !== this.currentFormState.value.latitude ||
      !this.currentFormState.value.longitude ||
      data.longitude !== this.currentFormState.value.longitude;
    if (!!data && isSelectedMapLocationDifferentToOldMapLocation) {
      this.appState$.dispatch(
        new SetValueAction(this.currentFormState.id, {
          ...this.currentFormState.value,
          longitude: data.longitude,
          latitude: data.latitude,
        })
      );
      if (!isInitialLoading) {
        this.setFormStateDirty();
      }
    }
  }

  public setStationId(stationId: string): void {
    !!stationId &&
      this.appState$.dispatch(
        new SetValueAction(this.currentFormState.id, {
          ...this.currentFormState.value,
          stationId: stationId,
        })
      );
  }

  public resetCoords(): void {
    this.appState$.dispatch(
      new SetValueAction(this.currentFormState.id, {
        ...this.currentFormState.value,
        longitude: null,
        latitude: null,
      })
    );
  }

  public resetStationId() {
    this.appState$.dispatch(
      new SetValueAction(this.currentFormState.id, {
        ...this.currentFormState.value,
        stationId: null,
      })
    );
  }

  public resetFailureLocationValues() {
    this.gridFailureStations = null;
    this.gridFailureStations = [];
    this.appState$.dispatch(
      new SetValueAction(this.currentFormState.id, {
        ...this.currentFormState.value,
        stationDescription: null,
        stationId: null,
        stationIds: null,
        addressPolygonPoints: null,
        latitude: null,
        longitude: null,
        postcode: null,
        district: null,
        city: null,
        street: null,
        housenumber: null,
        radius: null,
        radiusId: null,
        freetextCity: null,
        freetextDistrict: null,
        freetextPostcode: null,
      })
    );
  }

  public setViewStateForReqProps(part: string): void {
    switch (part) {
      case Globals.FAILURE_LOCATION_NS:
        this.appState$.dispatch(new EnableAction(INITIAL_STATE.controls.city.id));
        this.appState$.dispatch(new EnableAction(INITIAL_STATE.controls.radiusId.id));
        this._disableStationControls();

        break;
      case Globals.FAILURE_LOCATION_MS:
        this._disableAddressControls();
        break;
      case Globals.FAILURE_LOCATION_MAP:
        this._disableAddressControls();
        this._disableStationControls();
        this.appState$.dispatch(new EnableAction(INITIAL_STATE.controls.radiusId.id));
        break;
      default:
        break;
    }
  }

  public setPublicationChannels(channel: string, checked: boolean) {
    if (checked) {
      this.appState$.dispatch(gridFailureActions.createPublicationChannelAssignment({ gridFailureId: this._gridFailureId, channel: channel }));
    } else {
      this.appState$.dispatch(gridFailureActions.deletePublicationChannelAssignment({ gridFailureId: this._gridFailureId, channel: channel }));
    }
  }

  public setSelectedStation(selectedStation: models.FailureStation) {
    this._selectedStation = selectedStation;
  }

  public postSelectedStation() {
    if (!this._selectedStation) {
      return;
    }
    if (
      (this.currentFormState.value.stationIds &&
        unbox(this.currentFormState.value.stationIds) &&
        unbox(this.currentFormState.value.stationIds).some(id => id === this._selectedStation.id)) ||
      (this.gridFailureStations && this.gridFailureStations.some(station => station.id === this._selectedStation.id))
    ) {
      this._utilService.displayNotification('SelectedStationAlreadyAssigned', 'alert');
      return;
    }

    this.gridFailureStations = [...this.gridFailureStations, this._selectedStation];

    if (this.gridFailureStations && !!this.gridFailureStations.length) {
      this.latLonMapping({ longitude: this.gridFailureStations[0].longitude, latitude: this.gridFailureStations[0].latitude }, false);

      this._calculatePolygon(this.gridFailureStations.map(station => station.id));
      this.appState$.dispatch(new MarkAsDirtyAction(this.currentFormState.id));
    }
    this._selectedStation = null;
  }

  public deleteGridFailureStation(stationId: string) {
    if (this.currentFormState.isDisabled) return;
    // reset coords if stations are empty after whole transaction
    if (this.gridFailureStations.length === 1) {
      this.resetCoords();
    }
    this.gridFailureStations = this.gridFailureStations.filter(station => station.id != stationId);
    this.appState$.dispatch(new SetValueAction(this.currentFormState.controls.stationIds.id, box(this.gridFailureStations.map(station => station.id))));

    this.appState$.dispatch(new MarkAsDirtyAction(this.currentFormState.id));

    if (this.gridFailureStations && !!this.gridFailureStations.length) {
      this.latLonMapping({ longitude: this.gridFailureStations[0].longitude, latitude: this.gridFailureStations[0].latitude }, false);
      this._calculatePolygon(this.gridFailureStations.map(station => station.id));
    }
  }

  public setFormStateDirty(): void {
    this.appState$.dispatch(new MarkAsDirtyAction(this.currentFormState.id));
  }

  public setFormStatePristine(): void {
    this.appState$.dispatch(new MarkAsPristineAction(this.currentFormState.id));
  }

  private _disableAddressControls(): void {
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.postcode.id));
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.city.id));
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.district.id));
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.street.id));
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.housenumber.id));
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.radiusId.id));
  }

  public loadAddressPostalcodes(): void {
    const branch = !!this.currentFormState ? this.currentFormState.value.branch : null;
    const community = !!this.currentFormState ? this.currentFormState.value.city : null;
    const district = !!this.currentFormState ? this.currentFormState.value.district : null;
    this.appState$.dispatch(gridFailureActions.loadAddressPostalcodes({ branch: branch, community: community, district: district }));
  }
  public loadAddressStreets(): void {
    const branch = !!this.currentFormState ? this.currentFormState.value.branch : null;
    const community = !!this.currentFormState ? this.currentFormState.value.city : null;
    const district = !!this.currentFormState ? this.currentFormState.value.district : null;
    const postcode = !!this.currentFormState ? this.currentFormState.value.postcode : null;
    this.appState$.dispatch(gridFailureActions.loadAddressStreets({ branch: branch, postcode: postcode, community: community, district: district }));
  }

  public loadGridFailureStations(gridFailureId?: string) {
    this.appState$.dispatch(gridFailureActions.loadGridFailureStations({ payload: gridFailureId || this._gridFailureId }));
  }

  public checkForMaxVersion(): boolean {
    return this.currentFormState.value.versionNumber === this.maxVersionNumber;
  }

  public showForStatus(): boolean {
    return this.currentFormState.value.statusIntern !== StateEnum.COMPLETED && this.currentFormState.value.statusIntern !== StateEnum.CANCELED;
  }

  public createDistributionGroupAssignment(selectedDistributionGroup: models.DistributionGroup) {
    if (selectedDistributionGroup) {
      let testGroup: models.DistributionGroup = this._assignedDistributionGroups.find(group => group.id === selectedDistributionGroup.id);
      if (testGroup) {
        this._utilService.displayNotification('SelectedDistributionGroupAlreadyAssigned', 'alert');
      } else {
        this.appState$.dispatch(
          gridFailureActions.createDistributionGroupAssignment({ gridFailureId: this._gridFailureId, newGroup: selectedDistributionGroup })
        );
      }
    }
  }

  public deleteDistributionGroupAssignment(groupId: string) {
    this.appState$.dispatch(gridFailureActions.deleteDistributionGroupAssignment({ gridFailureId: this._gridFailureId, groupId: groupId }));
  }

  private _disableStationControls(): void {
    this.appState$.dispatch(new DisableAction(INITIAL_STATE.controls.stationDescription.id));
  }

  private _loadInitialAddressData(formState: FormGroupState<models.GridFailure>) {
    if (!formState.value || !formState.controls) return;

    this.appState$.dispatch(
      gridFailureActions.loadAllAddressCommunities({
        branch: formState.value.branch,
      })
    );
    this.appState$.dispatch(
      gridFailureActions.loadAddressDistrictsOfCommunity({
        branch: formState.value.branch,
        community: formState.value.city,
      })
    );
    this.appState$.dispatch(
      gridFailureActions.loadAddressPostalcodes({
        branch: formState.value.branch,
        community: formState.value.city,
        district: formState.value.district,
      })
    );
    this.appState$.dispatch(
      gridFailureActions.loadAddressStreets({
        branch: formState.value.branch,
        postcode: formState.value.postcode,
        community: formState.value.city,
        district: formState.value.district,
      })
    );
    this.appState$.dispatch(
      gridFailureActions.loadAddressHouseNumbers({
        branch: formState.value.branch,
        postcode: formState.value.postcode,
        community: formState.value.city,
        street: formState.value.street,
      })
    );
  }

  private _setBranchIds(): void {
    this.gridFailureBranches$.subscribe((branches: models.FailureBranch[]) => {
      this.branches = branches;
      if (branches.length > 0) {
        this._setNoBranchId(branches);
        this._setGasBranchId(branches);
        this._setPowerBranchId(branches);
      }
    });
  }

  private _setNoBranchId(branches: models.FailureBranch[]): void {
    const noBranch: models.FailureBranch = branches.find(branch => branch.name === enums.BranchNameEnum.NO_BRANCH);
    this.noBranchId = !!noBranch ? noBranch.id : null;
    this.appState$.dispatch(new SetUserDefinedPropertyAction(FORM_ID, NO_BRANCH_ID_KEY, this.noBranchId));
  }

  private _setPowerBranchId(branches: models.FailureBranch[]): void {
    const powerBranch: models.FailureBranch = branches.find(branch => branch.name === enums.BranchNameEnum.POWER);
    this.powerBranchId = !!powerBranch ? powerBranch.id : null;
    this.appState$.dispatch(new SetUserDefinedPropertyAction(FORM_ID, POWER_BRANCH_ID_KEY, this.powerBranchId));
  }

  private _setGasBranchId(branches: models.FailureBranch[]): void {
    let gasBranchId: string;
    const gasBranch: models.FailureBranch = branches.find(branch => branch.name === enums.BranchNameEnum.GAS);
    gasBranchId = !!gasBranch ? gasBranch.id : null;
    this.appState$.dispatch(new SetUserDefinedPropertyAction(FORM_ID, GAS_BRANCH_ID_KEY, gasBranchId));
  }

  private _clear(): void {
    this.appState$.dispatch(new SetValueAction(FORM_ID, INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(FORM_ID));
    this.appState$.dispatch(new EnableAction(FORM_ID));
    this.oldVersion = false;
    this._gridFailureId = null;
    this.maxVersionNumber = null;
    this.gridFailureStations = [];
    this._assignedDistributionGroups = null;
    this.selectedPublicationChannels = null;
    navigateHome(this._router);
  }

  private _setDynamicRequired(): void {
    const isBranchSelected = !!this.currentFormState.controls && !!this.currentFormState.controls.branchId;
    this.isFieldRequiredDependingOnBranchId = isBranchSelected && this.currentFormState.controls.branchId.value !== this.noBranchId;
  }

  private _setGridFailureStations(versionNumber: string) {
    const stationsService = +versionNumber < this.maxVersionNumber ? this.histGridFailureStations$ : this.gridFailureStations$;
    this.gridFailureStations = null;
    stationsService.pipe(take(1), takeUntil(this._endSubscriptions$)).subscribe(stations => {
      this.gridFailureStations = stations;
      if (this.gridFailureStations && !!this.gridFailureStations.length) {
        this._calculatePolygon(this.gridFailureStations.map(station => station.id));
      }
    });
  }

  private _showButtonsByState(state: string): void {
    this.showQualifyButton = false;
    this.showStornoButton = false;
    this.showCreatedButton = false;
    this.showUpdateButton = false;
    this.showPublishButton = false;
    this.showCompleteButton = false;
    this.showDeleteButton = false;

    switch (state) {
      case enums.StateEnum.NEW:
        this.showDeleteButton = true;
        this.showCreatedButton = true;
        break;
      case enums.StateEnum.PLANNED:
        this.showDeleteButton = true;
        this.showCreatedButton = true;
        break;
      case enums.StateEnum.CREATED:
        this.showDeleteButton = true;
      case enums.StateEnum.UPDATED:
        this.showQualifyButton = true;
        this.showStornoButton = true;
        break;
      case enums.StateEnum.QUALIFIED:
        this.showUpdateButton = true;
        this.showPublishButton = true;
        this.showCompleteButton = true;
        this.showStornoButton = true;
        break;
      default:
        break;
    }
  }

  private _handleUserDefinedProperties(formState: FormGroupState<models.GridFailure>) {
    const userDefinedProperty = formState.userDefinedProperties[DEPENDENT_FIELD_KEY];
    switch (userDefinedProperty) {
      case formState.controls.__formBranch.id:
      case formState.id:
        this._loadInitialAddressData(formState);
        break;

      case formState.controls.postcode.id:
        this.loadAddressStreets();
        break;

      case formState.controls.city.id:
        this.loadGridFailureDistricts(formState.value.city, formState.value.branch);
        break;

      case formState.controls.district.id:
        this.loadAddressPostalcodes();
        break;

      case formState.controls.street.id:
        this.appState$.dispatch(
          gridFailureActions.loadAddressHouseNumbers({
            branch: formState.value.branch,
            postcode: formState.value.postcode,
            community: formState.value.city,
            street: formState.value.street,
          })
        );
        break;
      case formState.controls.housenumber.id:
        this._setLatLong(formState.controls.housenumber.value);
        break;

      default:
        break;
    }
  }

  private _reloadDetail() {
    if (this._configService.getEnv('env') !== 'test') {
      window.location.reload();
    }
  }
}
