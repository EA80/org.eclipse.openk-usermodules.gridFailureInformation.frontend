/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpService, Query, GET, Path, Adapter, PUT, POST, Body, DefaultHeaders, DELETE } from '@grid-failure-information-app/shared/async-services/http';
import { Observable, of } from 'rxjs';
import { GridFailureService } from '@grid-failure-information-app/pages/grid-failure/grid-failure.service';
import {
  GridFailure,
  FailureBranch,
  FailureClassification,
  FailureState,
  FailureExpectedReason,
  FailureRadius,
  FailureStation,
  FailureHousenumber,
  FailureAddress,
  DistributionGroup,
  PublicationChannel,
  Polygon,
} from '@grid-failure-information-app/shared/models';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class GridFailureApiClient extends HttpService {
  @GET('/grid-failure-informations')
  @Adapter(GridFailureService.gridAdapter)
  public getGridFailures(): Observable<GridFailure[]> {
    return null;
  }

  @GET('/grid-failure-informations/{id}')
  @Adapter(GridFailureService.itemAdapter)
  public getGridFailureDetails(@Path('id') gridFailureId: string): Observable<GridFailure> {
    return null;
  }

  @PUT('/grid-failure-informations/{id}')
  @Adapter(GridFailureService.itemAdapter)
  public putGridFailure(@Path('id') id: string, @Body() gridFailure: GridFailure, @Query('saveForPublish') saveForPublish: boolean): Observable<GridFailure> {
    return null;
  }

  @POST('/grid-failure-informations')
  @Adapter(GridFailureService.itemAdapter)
  public postGridFailure(@Body() newGridFailure: GridFailure): Observable<GridFailure> {
    return null;
  }

  @DELETE('/grid-failure-informations/{gridFailureId}')
  @Adapter(GridFailureService.gridAdapter)
  public deleteGridFailure(@Path('gridFailureId') gridFailureDetailId: string): Observable<void> {
    return null;
  }

  @GET('/hist-grid-failure-informations/{id}/versions')
  @Adapter(GridFailureService.gridAdapter)
  public getGridFailureVersions(@Path('id') id: string): Observable<GridFailure[]> {
    return null;
  }

  @GET('/hist-grid-failure-informations/{id}/versions/{versionNumber}')
  @Adapter(GridFailureService.itemAdapter)
  public getGridFailureVersion(@Path('id') id: string, @Path('versionNumber') versionNumber: number): Observable<GridFailure> {
    return null;
  }

  @GET('/branches')
  @Adapter(GridFailureService.branchListAdapter)
  public getGridFailureBranches(): Observable<FailureBranch[]> {
    return null;
  }

  @GET('/failure-classifications')
  @Adapter(GridFailureService.classificationListAdapter)
  public getGridFailureClassifications(): Observable<FailureClassification[]> {
    return null;
  }

  @GET('/status')
  @Adapter(GridFailureService.stateListAdapter)
  public getGridFailureStates(): Observable<FailureState[]> {
    return null;
  }

  @GET('/radii')
  @Adapter(GridFailureService.radiusListAdapter)
  public getGridFailureRadii(): Observable<FailureRadius[]> {
    return null;
  }

  @POST('/grid-failure-informations/condense')
  @Adapter(GridFailureService.itemAdapter)
  public postGridFailuresCondensation(@Body() list: string[]): Observable<GridFailure> {
    return null;
  }

  @GET('/grid-failure-informations/condensed/{id}')
  @Adapter(GridFailureService.gridAdapter)
  public getCondensedGridFailures(@Path('id') gridFailureId: string): Observable<GridFailure[]> {
    return null;
  }

  @PUT('/grid-failure-informations/{gridFailureId}/condense')
  @Adapter(GridFailureService.gridAdapter)
  public putGridFailuresCondensation(@Path('gridFailureId') gridFailureId: string, @Body() list: string[]): Observable<GridFailure[]> {
    return null;
  }

  @GET('/stations')
  @Adapter(GridFailureService.stationListAdapter)
  public getStations(): Observable<FailureStation[]> {
    return null;
  }

  @POST('/stations/polygon-coordinates')
  @Adapter(GridFailureService.polygonAdapter)
  public getGridFailurePolygon(@Body() stationIds: string[]): Observable<Array<[number, number]>> {
    return null;
  }

  @GET('/grid-failure-informations/{gridFailureId}/stations')
  @Adapter(GridFailureService.stationListAdapter)
  public getGridFailureStations(@Path('gridFailureId') gridFailureId: string): Observable<FailureStation[]> {
    return null;
  }

  @GET('/hist-grid-failure-informations/{gridFailureId}/versions/{versionNumber}/stations')
  @Adapter(GridFailureService.stationListAdapter)
  public getHistGridFailureStations(@Path('gridFailureId') gridFailureId: string, @Path('versionNumber') versionNumber: string): Observable<FailureStation[]> {
    return null;
  }

  @POST('/grid-failure-informations/{gridFailureDetailId}/stations')
  @Adapter(GridFailureService.stationListAdapter)
  public postGridFailureStation(@Path('gridFailureDetailId') gridFailureDetailId: string, @Body() station: FailureStation): Observable<FailureStation[]> {
    return null;
  }

  @DELETE('/grid-failure-informations/{gridFailureId}/stations/{gridFailureStationId}')
  @Adapter(GridFailureService.stationAdapter)
  public deleteGridFailureStation(
    @Path('gridFailureId') gridFailureDetailId: string,
    @Path('gridFailureStationId') gridFailureStationId: string
  ): Observable<void> {
    return null;
  }

  @GET('/addresses/postcodes')
  @Adapter(GridFailureService.addressItemListAdapter)
  public getAddressPostalcodes(
    @Query('branch') branch: string,
    @Query('community') community: string,
    @Query('district') district: string,
    ): Observable<string[]> {
    return null;
  }

  @GET('/addresses/communities/{postcode}')
  @Adapter(GridFailureService.addressItemListAdapter)
  public getAddressCommunities(@Path('postcode') postcode: string, @Query('branch') branch: string): Observable<string[]> {
    return null;
  }

  @GET('/addresses/communities')
  @Adapter(GridFailureService.addressItemListAdapter)
  public getAllAddressCommunities(@Query('branch') branch: string): Observable<string[]> {
    return null;
  }

  @GET('/addresses/districts/{community}')
  @Adapter(GridFailureService.addressItemListAdapter)
  public getAddressDistrictsOfCommunity(
    @Path('community') community: string,
    @Query('branch') branch: string
  ): Observable<string[]> {
    return null;
  }

  @GET('/addresses/districts')
  @Adapter(GridFailureService.addressItemListAdapter)
  public getAddressDistricts(
    @Query('postcode') postcode: string,
    @Query('community') community: string,
    @Query('branch') branch: string
  ): Observable<string[]> {
    return null;
  }

  @GET('/addresses/streets')
  @Adapter(GridFailureService.addressItemListAdapter)
  public getAddressStreets(
    @Query('postcode') postcode: string,
    @Query('community') community: string,
    @Query('district') district: string,
    @Query('branch') branch: string
  ): Observable<string[]> {
    return null;
  }
  @GET('/expected-reasons')
  @Adapter(GridFailureService.expectedReasonListAdapter)
  public getGridFailureExpectedReasons(@Query('branch') branch: string): Observable<FailureExpectedReason[]> {
    return null;
  }
  @GET('/addresses/housenumbers')
  @Adapter(GridFailureService.housenumberListAdapter)
  public getAddressHousenumbers(
    @Query('postcode') postcode: string,
    @Query('community') community: string,
    @Query('street') street: string,
    @Query('branch') branch: string
  ): Observable<FailureHousenumber[]> {
    return null;
  }

  @GET('/addresses/{uuid}')
  @Adapter(GridFailureService.addressAdapter)
  public getAddress(@Path('uuid') uuid: string): Observable<FailureAddress> {
    return null;
  }

  @GET('/grid-failure-informations/{gridFailureId}/distribution-groups')
  @Adapter(GridFailureService.distributionGroupListAdapter)
  public getGridFailureDistributionGroups(@Path('gridFailureId') gridFailureId: string): Observable<DistributionGroup[]> {
    return null;
  }

  @POST('/grid-failure-informations/{gridFailureId}/distribution-groups')
  @Adapter(GridFailureService.distributionGroupAdapter)
  public postDistributionGroupAssignment(
    @Path('gridFailureId') gridFailureId: string,
    @Body() groupToAssign: DistributionGroup
  ): Observable<DistributionGroup> {
    return null;
  }

  @DELETE('/grid-failure-informations/{gridFailureId}/distribution-groups/{groupId}')
  @Adapter(GridFailureService.distributionGroupAdapter)
  public deleteDistributionGroupAssignment(@Path('gridFailureId') gridFailureId: string, @Path('groupId') groupId: string): Observable<DistributionGroup> {
    return null;
  }

  @GET('/grid-failure-informations/{gridFailureId}/channels')
  @Adapter(GridFailureService.publicationChannelListAdapter)
  public getGridFailurePublicationChannels(@Path('gridFailureId') gridFailureId: string): Observable<PublicationChannel[]> {
    return null;
  }

  @POST('/grid-failure-informations/{gridFailureId}/channels')
  @Adapter(GridFailureService.publicationChannelAdapter)
  public postPublicationChannelAssignment(
    @Path('gridFailureId') gridFailureId: string,
    @Query('publicationChannel') publicationChannel: string
  ): Observable<PublicationChannel> {
    return null;
  }

  @DELETE('/grid-failure-informations/{gridFailureId}/channels')
  @Adapter(GridFailureService.publicationChannelAdapter)
  public deletePublicationChannelAssignment(
    @Path('gridFailureId') gridFailureId: string,
    @Query('publicationChannel') publicationChannel: string
  ): Observable<PublicationChannel> {
    return null;
  }

  @GET('/failure-reminder')
  @Adapter(GridFailureService.reminderAdapter)
  public getFailureReminder(): Observable<boolean> {
    return null;
  }
}
