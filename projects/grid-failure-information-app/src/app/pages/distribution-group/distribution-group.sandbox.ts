/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { BaseSandbox } from '@grid-failure-information-app/shared/sandbox/base.sandbox';
import { Store, ActionsSubject, State } from '@ngrx/store';
import * as store from '@grid-failure-information-app/shared/store';
import { Observable, Subject } from 'rxjs';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import * as distributionGroupFormReducer from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-details-form.reducer';
import { FormGroupState, MarkAsTouchedAction, ResetAction, SetValueAction } from 'ngrx-forms';
import { debounceTime, distinctUntilChanged, map, takeUntil, switchMap, take } from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SafetyQueryDialogComponent } from '@grid-failure-information-app/shared/components/dialogs/safety-query-dialog/safety-query-dialog.component';
import { ofType } from '@ngrx/effects';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';
import { CellClickedEvent } from 'ag-grid-community';
import { DistributionGroup, DistributionGroupMember, Contact, DistributionGroupTextPlaceholder } from '@grid-failure-information-app/shared/models';
import { DistributionPublicationStatusEnum } from '@grid-failure-information-app/shared/constants/enums';
import { INITIAL_STATE } from '@grid-failure-information-app/shared/store/reducers/distribution-groups/distribution-group-details-form.reducer';
import * as FileSaver from 'file-saver';
import { InitialEmailContent } from '@grid-failure-information-app/shared/models/settings-initial-email-content.model';

export interface PostcodeInterface {
  postcode: string;
}

@Injectable()
export class DistributionGroupSandbox extends BaseSandbox {
  public distributionGroupList$: Observable<DistributionGroup[]> = this.appState$.select(store.getDistributionGroupsData);
  public distributionGroupLoading$: Observable<boolean> = this.appState$.select(store.getDistributionGroupsLoading);
  public distributionGroupDetailsFormState$: Observable<FormGroupState<DistributionGroup>> = this.appState$.select(store.getDistributionGroupsDetails);
  public currentFormState: FormGroupState<DistributionGroup>;
  public distributionGroupMember$: Observable<DistributionGroupMember[]> = this.appState$.select(store.getDistributionGroupMembersData).pipe();
  public distributionGroupMemberLoading$: Observable<boolean> = this.appState$.select(store.getDistributionGroupMembersLoading);
  public disableMemberButton: boolean = false;
  public distributionGroupTextPlaceholder: DistributionGroupTextPlaceholder;
  public distributionTemplate = DistributionPublicationStatusEnum;
  public selectedTemplate: string = DistributionPublicationStatusEnum.PUBLISH;
  public oldSelectedTemplate: string;
  public selectedMemberRowIndex: number = 0;
  public postcodes: PostcodeInterface[] = [];
  public selectedDistributionGroup: DistributionGroup;
  public contactModelChangedSubject$: Subject<any> = new Subject();

  private _selectedContact: Contact;
  private _distributionGroupMembers: Array<DistributionGroupMember>;
  private _selectFirst: boolean = true;
  private _selectedDistributionGroupMember: DistributionGroupMember;

  constructor(
    protected appState$: Store<store.State>,
    public actionsSubject: ActionsSubject,
    private _utilService: UtilService,
    private _modalService: NgbModal
  ) {
    super(appState$);
    this.registerEvents();
  }

  public createDistributionGroup(): void {
    this.appState$.dispatch(distributionGroupActions.resetDistributionGroupMembers());
    this._clear();
    this.appState$.dispatch(new MarkAsTouchedAction(distributionGroupFormReducer.FORM_ID));
    this.selectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
    this.oldSelectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
    this.appState$.select(store.getInitialEmailContent).subscribe((initialEmailContent: InitialEmailContent) => {
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectView.id, initialEmailContent.emailSubjectPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublish.id, initialEmailContent.emailSubjectPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdate.id, initialEmailContent.emailSubjectUpdateInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectComplete.id, initialEmailContent.emailSubjectCompleteInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextView.id, initialEmailContent.emailContentPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublish.id, initialEmailContent.emailContentPublishInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdate.id, initialEmailContent.emailContentUpdateInit));
      this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextComplete.id, initialEmailContent.emailContentCompleteInit));
    });
    this.clearAssignmentInput();
    this.disableMemberButton = true;
    this.postcodes = [];
  }

  public loadDistributionGroups(): void {
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroups());
  }

  public loadDistributionGroupDetail(id: string): void {
    this.selectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroupsDetail({ payload: id }));
  }

  public loadDistributionGroupTextPlaceholders(): void {
    this.appState$.dispatch(distributionGroupActions.loadDistributionGroupTextPlaceholders());
  }

  public saveDistributionGroup(): void {
    // Copy user control value to right model property for correct validation
    switch (this.selectedTemplate) {
      case DistributionPublicationStatusEnum.PUBLISH:
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublish.id, this.currentFormState.value.emailSubjectView));
        break;
      case DistributionPublicationStatusEnum.COMPLETE:
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectComplete.id, this.currentFormState.value.emailSubjectView));
        break;
      case DistributionPublicationStatusEnum.UPDATE:
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdate.id, this.currentFormState.value.emailSubjectView));
        break;
    }
    if (this.currentFormState.isValid) {
      this.changeEmailTemplate(this.selectedTemplate, this.selectedTemplate);
      let distributionGroup = new DistributionGroup(this.currentFormState.value);

      // update: id exists
      if (distributionGroup && distributionGroup.id) {
        this.selectedDistributionGroup = distributionGroup;
      }
      this.actionsSubject
        .pipe(
          ofType(distributionGroupActions.saveDistributionGroupSuccess),
          take(1),
          map(action => action.payload),
          takeUntil(this._endSubscriptions$)
        )
        .subscribe(item => {
          //post case: new group with  is returned; put case: general response without id is returned
          if (item && item.id) {
            this.selectedDistributionGroup = item;
          }
          this.selectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
          this.oldSelectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
          this._clear();
        });

      this.appState$.dispatch(
        distributionGroupActions.saveDistributionGroup({
          payload: distributionGroup,
        })
      );
    } else {
      this._utilService.displayNotification('MandatoryFieldError', 'alert');
    }
  }

  public deleteDistributionGroup(id: string): void {
    const modalRef = this._modalService.open(SafetyQueryDialogComponent);
    modalRef.componentInstance.title = 'ConfirmDialog.Action.delete';
    modalRef.componentInstance.body = 'ConfirmDialog.Deletion';
    modalRef.result.then(
      () => {
        this.actionsSubject.pipe(ofType(distributionGroupActions.deleteDistributionGroupSuccess), take(1)).subscribe(() => {
          this._selectFirst = true;
        });
        this.appState$.dispatch(distributionGroupActions.deleteDistributionGroup({ payload: id }));
      },
      () => {}
    );
  }

  public deleteDistributionGroupMember(groupId: string, memberId: string): void {
    this.actionsSubject.pipe(ofType(distributionGroupActions.deleteDistributionGroupMemberSuccess), take(1)).subscribe(() => (this.selectedMemberRowIndex = 0));
    this.appState$.dispatch(distributionGroupActions.deleteDistributionGroupMember({ groupId: groupId, memberId: memberId }));
  }

  public updateDistributionGroupMember(postcodeList: string[]): void {
    let displayMainAddress: string = null;
    if (
      this._selectedDistributionGroupMember &&
      this._selectedDistributionGroupMember.mainAddress &&
      this._selectedDistributionGroupMember.mainAddress.trim().length > 0
    ) {
      displayMainAddress = this._selectedDistributionGroupMember.mainAddress;
    }
    this._selectedDistributionGroupMember = {
      ...this._selectedDistributionGroupMember,
      postcodeList: postcodeList,
      displayMainAddress: displayMainAddress,
    };
    this.appState$.dispatch(
      distributionGroupActions.updateDistributionGroupMember({
        groupId: this.selectedDistributionGroup.id,
        memberId: this._selectedDistributionGroupMember.id,
        member: this._selectedDistributionGroupMember,
      })
    );
  }

  public registerEvents(): void {
    this.distributionGroupDetailsFormState$.pipe(takeUntil(this._endSubscriptions$)).subscribe((formState: FormGroupState<DistributionGroup>) => {
      this.currentFormState = formState;
    });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.loadDistributionGroupsSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((groups: Array<DistributionGroup>) => {
        if (groups && groups.length > 0) {
          if (!!this.selectedDistributionGroup && !this._selectFirst) {
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupsDetail({ payload: this.selectedDistributionGroup.id }));
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: this.selectedDistributionGroup.id }));
          } else {
            this.selectedDistributionGroup = groups[0];
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupsDetail({ payload: groups[0].id }));
            this.appState$.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: groups[0].id }));
            this._selectFirst = false;
          }
        }
      });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.loadDistributionGroupMembersSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((members: Array<DistributionGroupMember>) => {
        this._distributionGroupMembers = members;
        if (!this._selectedDistributionGroupMember || this.selectedMemberRowIndex === 0) {
          this._selectedDistributionGroupMember = this._distributionGroupMembers[0];
        }
        this.transformPostcodes();
      });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.loadDistributionGroupTextPlaceholdersSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((item: DistributionGroupTextPlaceholder) => {
        this.distributionGroupTextPlaceholder = item;
      });
    this.actionsSubject
      .pipe(
        ofType(distributionGroupActions.exportContactsSuccess),
        map(action => action.payload),
        takeUntil(this._endSubscriptions$)
      )
      .subscribe((response: any) => {
        let filename: string;
        let defaultFilename: string = 'export.txt';
        let filenameContentDisposition: string;
        const responseBody = new Blob([response._body], { type: 'text/csv;charset=utf-8;' });
        if (response.headers) {
          const contentDisposition = response.headers.get('content-disposition');
          filenameContentDisposition = contentDisposition.split(';')[1].trim().split('=')[1].replace(/"/g, '');
        }
        filename = filenameContentDisposition ? filenameContentDisposition : defaultFilename;
        FileSaver.saveAs(responseBody, filename);
      });
  }

  public showMembersToSelectedGroup(selectedGroup: CellClickedEvent): void {
    if (selectedGroup && selectedGroup.data) {
      this.selectedDistributionGroup = selectedGroup.data;
      this.appState$.dispatch(distributionGroupActions.loadDistributionGroupMembers({ payload: selectedGroup.data.id }));
    }
  }

  public loadContacts(term: string): Observable<any> {
    this.appState$.dispatch(distributionGroupActions.loadContacts({ searchText: term }));
    return this.actionsSubject.pipe(
      ofType(distributionGroupActions.loadContactsSuccess),
      map(action => action.payload),
      takeUntil(this._endSubscriptions$)
    );
  }

  public searchForContacts = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => (term.length < 2 ? [] : this.loadContacts(term)))
    );

  public formatter = (s: Contact | string) => {
    if (s instanceof Contact) return s.contactSearchString;
    else return s;
  };

  public setSelectedContact(selectedContact: Contact): void {
    this._selectedContact = selectedContact;
  }

  public assignContactToGroup(): void {
    if (!this.selectedDistributionGroup) return;
    if (!this._selectedContact) {
      this._utilService.displayNotification('NoContactSelected');
      return;
    }
    let testMember: DistributionGroupMember = this._distributionGroupMembers.find(member => member.contactId === this._selectedContact.uuid);
    if (testMember) {
      this._utilService.displayNotification('SelectedContactAlreadyAssigned', 'alert');
      this.setSelectedContact(undefined);
    } else {
      let newGroupMemberData = {
        contactId: this._selectedContact.uuid,
        distributionGroupUuid: this.selectedDistributionGroup.id,
      };
      const newGroupMember = new DistributionGroupMember(newGroupMemberData);
      this.actionsSubject.pipe(ofType(distributionGroupActions.createDistributionGroupMemberSuccess), take(1)).subscribe(payload => {
        this.selectedMemberRowIndex = 0;
      });
      this.appState$.dispatch(
        distributionGroupActions.createDistributionGroupMember({
          groupId: this.selectedDistributionGroup.id,
          newMember: newGroupMember,
        })
      );
      this.postcodes = [];
      this._selectedDistributionGroupMember = newGroupMember;
      this.setSelectedContact(undefined);
    }
  }

  public exportContacts(): void {
    if (!this.selectedDistributionGroup) return;
    this.appState$.dispatch(
      distributionGroupActions.exportContacts({
        groupId: this.selectedDistributionGroup.id,
      })
    );
  }

  public displayPostcodeNotNumberNotification() {
    this._utilService.displayNotification('PostcodeNotNumber', 'alert');
  }

  private _clear(): void {
    this.appState$.dispatch(new SetValueAction(distributionGroupFormReducer.FORM_ID, distributionGroupFormReducer.INITIAL_STATE.value));
    this.appState$.dispatch(new ResetAction(distributionGroupFormReducer.FORM_ID));
    this.disableMemberButton = false;
  }

  public changeEmailTemplate(templateValue: string, oldTemplateValue: string = DistributionPublicationStatusEnum.PUBLISH) {
    let emailsubject: string;
    let distributionText: string;
    let emailsubjectOld: string = this.currentFormState.value.emailSubjectView;
    let distributionTextOld: string = this.currentFormState.value.distributionTextView;

    switch (oldTemplateValue) {
      case DistributionPublicationStatusEnum.COMPLETE:
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectComplete.id, emailsubjectOld));
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextComplete.id, distributionTextOld));
        break;
      case DistributionPublicationStatusEnum.PUBLISH:
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectPublish.id, emailsubjectOld));
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextPublish.id, distributionTextOld));
        break;
      case DistributionPublicationStatusEnum.UPDATE:
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectUpdate.id, emailsubjectOld));
        this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextUpdate.id, distributionTextOld));
        break;
      default:
        break;
    }
    switch (templateValue) {
      case DistributionPublicationStatusEnum.COMPLETE:
        emailsubject = this.currentFormState.value.emailSubjectComplete;
        distributionText = this.currentFormState.value.distributionTextComplete;
        break;
      case DistributionPublicationStatusEnum.PUBLISH:
        emailsubject = this.currentFormState.value.emailSubjectPublish;
        distributionText = this.currentFormState.value.distributionTextPublish;
        break;
      case DistributionPublicationStatusEnum.UPDATE:
        emailsubject = this.currentFormState.value.emailSubjectUpdate;
        distributionText = this.currentFormState.value.distributionTextUpdate;
        break;
      default:
        break;
    }
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.emailSubjectView.id, emailsubject));
    this.appState$.dispatch(new SetValueAction(INITIAL_STATE.controls.distributionTextView.id, distributionText));
  }

  public transformPostcodes() {
    let postcodeStringArray: string[] = [];
    if (this._selectedDistributionGroupMember || (this._distributionGroupMembers && this._distributionGroupMembers.length > 0)) {
      postcodeStringArray = (this._selectedDistributionGroupMember ? this._selectedDistributionGroupMember : this._distributionGroupMembers[0]).postcodeList;
    }
    if (postcodeStringArray.length > 0) {
      let postcodes: PostcodeInterface[] = [];
      for (var index in postcodeStringArray) {
        postcodes.push({ postcode: postcodeStringArray[index] });
      }
      this.postcodes = postcodes;
    } else {
      this.postcodes = [];
    }
  }

  public setSelectedDistributionGroupMember(member: DistributionGroupMember): void {
    this._selectedDistributionGroupMember = member;
  }

  public deletePostcode(postcodeString: string): void {
    this.postcodes = this.postcodes.filter(item => item.postcode != postcodeString);
    this._updateMember(this.postcodes);
  }

  public addPostcode(input: PostcodeInterface) {
    let postcodeAlreadyAssigned = false;
    if (this.postcodes.length > 0) {
      for (var index in this.postcodes) {
        if (this.postcodes[index].postcode === input.postcode) {
          postcodeAlreadyAssigned = true;
        }
      }
    }
    if (postcodeAlreadyAssigned) {
      this._utilService.displayNotification('PostcodeAlreadyAssigned', 'alert');
    } else {
      this.postcodes = [...this.postcodes, input];
      this._updateMember(this.postcodes);
    }
  }

  public getDistributionGroupMemebers(): Array<DistributionGroupMember> {
    return this._distributionGroupMembers ? this._distributionGroupMembers : [];
  }

  public clearAssignmentInput(): void {
    this.appState$.dispatch(distributionGroupActions.clearAssignmentInput());
  }

  private _updateMember(postcodes: PostcodeInterface[]) {
    let postcodeStringArray: string[] = [];
    if (postcodes.length > 0) {
      for (var index in postcodes) {
        postcodeStringArray.push(postcodes[index].postcode);
      }
    }
    this.updateDistributionGroupMember(postcodeStringArray);
  }
}
