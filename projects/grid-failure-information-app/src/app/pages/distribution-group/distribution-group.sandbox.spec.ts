/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { Store, ActionsSubject } from '@ngrx/store';
import * as store from '@grid-failure-information-app/shared/store';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import { Contact, DistributionGroupMember, DistributionGroup } from '@grid-failure-information-app/shared/models';
import { DistributionPublicationStatusEnum } from '@grid-failure-information-app/shared/constants/enums';

describe('DistributionGroupSandbox', () => {
  interface PostcodeInterface {
    postcode: string;
  }

  let service: DistributionGroupSandbox;
  let appState: Store<store.State>;
  let actionsSubject: ActionsSubject;
  let utilService: UtilService;
  let modalService: NgbModal;
  let spyAppStateDispatch;

  beforeEach(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    actionsSubject = { dispatch: () => {}, pipe: () => of(true), select: () => of(true) } as any;
    utilService = { displayNotification() {} } as any;
    modalService = { open() {} } as any;
    spyAppStateDispatch = spyOn(appState, 'dispatch').and.callFake(() => {});

    service = new DistributionGroupSandbox(appState, actionsSubject, utilService, modalService);
    service['selectedDistributionGroup'] = new DistributionGroup({ id: 'x', emailSubjectView: 'emailSubjectView init' });
  });

  it('should create DistributionGroupSandbox service', () => {
    expect(service).toBeTruthy();
  });

  it('should set displayForm', () => {
    service.createDistributionGroup();
    expect(service.disableMemberButton).toBeTruthy();
  });

  it('should dispatch loadDistributionGroups Action via loadDistributionGroups()', () => {
    service.loadDistributionGroups();
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.loadDistributionGroups());
  });

  it('should dispatch loadDistributionGroupDetails Action via loadDistributionGroupDetail()', () => {
    service.loadDistributionGroupDetail('x');
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.loadDistributionGroupsDetail({ payload: 'x' }));
  });

  it('should dispatch loadDistributionGroupTextPlaceholders Action via loadDistributionGroupTextPlaceholders()', () => {
    service.loadDistributionGroupTextPlaceholders();
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.loadDistributionGroupTextPlaceholders());
  });

  it('should call dispatch for saving an distribution group if a valid form state is provided', () => {
    const spy: any = spyOn(service, 'saveDistributionGroup').and.callFake(() => {});
    service.currentFormState = { isValid: true } as any;
    service.saveDistributionGroup();
    expect(spy).toHaveBeenCalled();
  });

  it('should displayNotification if a invalid form state is provided and selectedTemplate is PUBLISH', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification').and.callThrough();
    service.currentFormState = {
      isValid: false,
      value: {
        emailSubjectView: 'subject value',
      },
    } as any;
    service.selectedTemplate = DistributionPublicationStatusEnum.PUBLISH;
    service.saveDistributionGroup();
    expect(spy).toHaveBeenCalled();
  });

  it('should displayNotification if a invalid form state is provided and selectedTemplate is UPDATE', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification').and.callThrough();
    service.currentFormState = {
      isValid: false,
      value: {
        emailSubjectView: 'subject value',
      },
    } as any;
    service.selectedTemplate = DistributionPublicationStatusEnum.UPDATE;
    service.saveDistributionGroup();
    expect(spy).toHaveBeenCalled();
  });

  it('should displayNotification if a invalid form state is provided and selectedTemplate is COMPLETE', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification').and.callThrough();
    service.currentFormState = {
      isValid: false,
      value: {
        emailSubjectView: 'subject value',
      },
    } as any;
    service.selectedTemplate = DistributionPublicationStatusEnum.COMPLETE;
    service.saveDistributionGroup();
    expect(spy).toHaveBeenCalled();
  });

  it('should open modal before distribution group is deleted', () => {
    spyOn(service['_modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    service.deleteDistributionGroup('x');
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should dispatch an action via showMembersToSelectedGroup()', () => {
    service.showMembersToSelectedGroup({ data: { id: 'x' } } as any);
    expect(appState.dispatch).toHaveBeenCalled();
  });

  it('should not dispatch an action via showMembersToSelectedGroup() when no selectedGroup is provided', () => {
    service.showMembersToSelectedGroup(undefined);
    expect(appState.dispatch).not.toHaveBeenCalled();
  });

  it('should dispatch loadContacts action via loadContacts()', () => {
    service.loadContacts('x');
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.loadContacts({ searchText: 'x' }));
  });

  it('should call loadContacts() via searchForContacts', () => {
    const spy: any = spyOn(service, 'loadContacts');
    service.searchForContacts(of('xx'));
    expect(spy).toHaveBeenCalled;
  });

  it('should return formatted argument', () => {
    expect(service.formatter('x')).toBe('x');
    let contact: Contact = new Contact({ name: 'test', contactSearchString: 'y' });
    expect(service.formatter(contact)).toBe(contact.contactSearchString);
  });

  it('should displayNotification when member already assigned', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification');
    service['_distributionGroupMembers'] = [new DistributionGroupMember({ contactId: 'x' })];
    service['_selectedContact'] = new Contact({ uuid: 'x' });
    service.assignContactToGroup();
    expect(spy).toHaveBeenCalled;
  });

  it('should displayNotification when no contact was selected', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification');
    service['_selectedContact'] = undefined;
    service.assignContactToGroup();
    expect(spy).toHaveBeenCalled;
  });

  it('should setSelectedContact()', () => {
    let test = new Contact();
    service.setSelectedContact(test);
    expect(service['_selectedContact']).toEqual(test);
  });

  it('should _clear form via clear()', () => {
    service['_clear']();
    expect(service.disableMemberButton).toBe(false);
  });

  it('should deleteDistributionGroupMember via deleteDistributionGroupMember action', () => {
    service.deleteDistributionGroupMember('x', 'y');
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.deleteDistributionGroupMember({ groupId: 'x', memberId: 'y' }));
  });

  it('should set complete email text and subject to view binded properties in changeEmailTemplate() ', () => {
    const formState = {
      value: {
        distributionTextView: '',
        emailSubjectView: '',
      },
    };
    service['currentFormState' as any] = formState;

    service.changeEmailTemplate('complete', 'complete');
    expect(spyAppStateDispatch).toHaveBeenCalledTimes(4);
  });

  it('should set update email text and subject to view binded properties in changeEmailTemplate() ', () => {
    const formState = {
      value: {
        distributionTextView: '',
        emailSubjectView: '',
      },
    };
    service['currentFormState' as any] = formState;

    service.changeEmailTemplate('update', 'update');
    expect(spyAppStateDispatch).toHaveBeenCalledTimes(4);
  });

  it('should set publish email text and subject to view binded properties in changeEmailTemplate() ', () => {
    const formState = {
      value: {
        distributionTextView: '',
        emailSubjectView: '',
      },
    };
    service['currentFormState' as any] = formState;

    service.changeEmailTemplate('publish', 'publish');
    expect(spyAppStateDispatch).toHaveBeenCalledTimes(4);
  });

  it('should make no changes in changeEmailTemplate() ', () => {
    const formState = {
      value: {
        distributionTextView: '',
        emailSubjectView: '',
      },
    };
    service['currentFormState' as any] = formState;

    service.changeEmailTemplate('', '');
    expect(spyAppStateDispatch).toHaveBeenCalledTimes(2);
  });

  it('should exportContacts via exportContacts action', () => {
    service['selectedDistributionGroup'] = new DistributionGroup({ id: 'x' });
    service.exportContacts();
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.exportContacts({ groupId: 'x' }));
  });

  it('should not exportContacts via exportContacts action when no selectedDistributionGroup is provided', () => {
    service['selectedDistributionGroup'] = undefined;
    service.exportContacts();
    expect(appState.dispatch).not.toHaveBeenCalled();
  });

  it('should displayNotification via displayPostcodeNotNumberNotification()', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification');
    service.displayPostcodeNotNumberNotification();
    expect(service['_utilService'].displayNotification).toHaveBeenCalled();
  });

  it('should update distributionGroupMember', () => {
    const postcodeList = ['test'];
    service.updateDistributionGroupMember(postcodeList);
    expect(appState.dispatch).toHaveBeenCalled();
  });

  it('should update distributionGroupMember including mainAddress', () => {
    const postcodeList = ['test'];
    const member = new DistributionGroupMember({ id: 'x', mainAddress: 'test' });
    service.setSelectedDistributionGroupMember(member);
    service.updateDistributionGroupMember(postcodeList);
    expect(service['_selectedDistributionGroupMember'].mainAddress).toEqual('test');
  });

  it('should set _selectedDistributionGroupMember', () => {
    const member = new DistributionGroupMember({ id: 'x' });
    service.setSelectedDistributionGroupMember(member);
    expect(service['_selectedDistributionGroupMember']).toEqual(member);
  });

  it('should updateMemeber for deleting a postcode', () => {
    const spy: any = spyOn(service as any, '_updateMember');
    service.deletePostcode('test');
    expect(service['_updateMember']).toHaveBeenCalled();
  });

  it('should call appropriate function for updating member', () => {
    const spy: any = spyOn(service, 'updateDistributionGroupMember');
    let postcodes: PostcodeInterface[] = [{ postcode: '12345' }];
    service['_updateMember'](postcodes);
    expect(spy).toHaveBeenCalled();
  });

  it('should call not function for updating member if no postcodes are provided', () => {
    const spy: any = spyOn(service, 'updateDistributionGroupMember');
    let postcodes: PostcodeInterface[] = [];
    service['_updateMember'](postcodes);
    expect(spy).toHaveBeenCalledWith([]);
  });

  it('should updateMember to add a postcode', () => {
    let input = { postcode: '12345' };
    service.postcodes = [];
    const spy: any = spyOn(service as any, '_updateMember');
    service.addPostcode(input);
    expect(service['_updateMember']).toHaveBeenCalled();
  });

  it('should display a notification if postcode is already assigned', () => {
    let input = { postcode: '12345' };
    service.postcodes = [{ postcode: '12345' }];
    const spy: any = spyOn(service['_utilService'], 'displayNotification');
    service.addPostcode(input);
    expect(spy).toHaveBeenCalled();
  });

  it('should updateMember to add a postcode', () => {
    let input = { postcode: '12345' };
    service.postcodes = [{ postcode: '11111' }];
    const spy: any = spyOn(service as any, '_updateMember');
    service.addPostcode(input);
    expect(service['_updateMember']).toHaveBeenCalled();
  });

  it('should transformPostcodes', () => {
    let testMember = new DistributionGroupMember({ id: '1', postcodeList: ['11111'] });
    service['_selectedDistributionGroupMember'] = testMember;
    service.transformPostcodes();
    expect(service.postcodes.length).toEqual(1);
  });

  it('should transformPostcodes', () => {
    let members = [new DistributionGroupMember({ id: '1', postcodeList: ['22222'] })];
    service['_distributionGroupMembers'] = members;
    service.transformPostcodes();
    expect(service.postcodes.length).toEqual(1);
  });

  it('should get DistributionGroupMemebers', () => {
    let members = [new DistributionGroupMember({ id: '1', postcodeList: ['11111'] })];
    service['_distributionGroupMembers'] = members;
    expect(service.getDistributionGroupMemebers()).toEqual(members);
  });

  it('should get empty array as DistributionGroupMemebers when no members are provided', () => {
    service['_distributionGroupMembers'] = null;
    expect(service.getDistributionGroupMemebers()).toEqual([]);
  });
});
