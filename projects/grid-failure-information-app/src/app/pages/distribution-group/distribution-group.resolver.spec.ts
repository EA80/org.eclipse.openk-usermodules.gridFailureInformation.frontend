/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupResolver } from '@grid-failure-information-app/pages/distribution-group/distribution-group.resolver';

describe('DistributionGroupResolver', () => {
  let component: DistributionGroupResolver;
  let sandbox: any;

  beforeEach(() => {
    sandbox = {
      clear() {},
      loadDistributionGroups() {},
      loadDistributionGroupTextPlaceholders() {},
    } as any;
  });

  beforeEach(() => {
    component = new DistributionGroupResolver(sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call loadDistributionGroups', () => {
    const spy = spyOn(sandbox, 'loadDistributionGroups');
    component.resolve();
    expect(spy).toHaveBeenCalled();
  });
});
