/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { BaseList } from '@grid-failure-information-app/shared/components/base-components/base.list';
import { DISTRIBUTION_GROUP_MEMBER_COLDEF } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group-members/distribution-group-members-col-def';
import { SetFilterComponent } from '@grid-failure-information-app/shared/filters/ag-grid/set-filter/set-filter.component';
import { GridApi } from 'ag-grid-community';
import { ofType } from '@ngrx/effects';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-distribution-group-members',
  templateUrl: './distribution-group-members.component.html',
  styleUrls: ['./distribution-group-members.component.scss'],
})
export class DistributionGroupMembersComponent extends BaseList implements OnInit {
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  @Output() rowSelectionChanged: EventEmitter<any> = new EventEmitter();
  public columnDefinition: any = DISTRIBUTION_GROUP_MEMBER_COLDEF;
  public frameworkComponents: { setFilterComponent: any };

  private _gridApi: GridApi | null | undefined;

  constructor(public sandbox: DistributionGroupSandbox) {
    super();
    this.frameworkComponents = { setFilterComponent: SetFilterComponent };
  }

  ngOnInit() {
    (this.gridOptions = {
      ...this.gridOptions,
      onRowClicked: event => {
        const rowChanged: boolean = this.sandbox.selectedMemberRowIndex !== event.rowIndex;
        this.sandbox.selectedMemberRowIndex = event.rowIndex;
        this.sandbox.setSelectedDistributionGroupMember(event.data);
        this.sandbox.transformPostcodes();
        if (rowChanged) this.rowSelectionChanged.emit(event.data.id);
      },
      onModelUpdated: event => {
        this.sandbox.contactModelChangedSubject$.next(true);
        if (event.api.getDisplayedRowAtIndex(this.sandbox.selectedMemberRowIndex)) {
          event.api.getDisplayedRowAtIndex(this.sandbox.selectedMemberRowIndex).setSelected(true);
        }
      },
    }),
      (this.gridOptions.context = {
        ...this.gridOptions.context,
        icons: { delete: true },
      });

    this.gridOptions.context.eventSubject.pipe(takeUntil(this._endSubscriptions$)).subscribe(event => {
      if (event.type === 'delete') {
        this.sandbox.deleteDistributionGroupMember(event.data.distributionGroupUuid, event.data.id);
      }
    });
    this.sandbox.actionsSubject.pipe(ofType(distributionGroupActions.clearAssignmentInput), takeUntil(this._endSubscriptions$)).subscribe(() => {
      this.clearSearchInput();
    });
  }

  public onGridReady(params): void {
    this._gridApi = params.api;
    const sortModel = [{ colId: 'name', sort: 'asc' }];
    this._gridApi.setSortModel(sortModel);
  }

  public clearSearchInput() {
    if (this.searchInput) {
      this.searchInput.nativeElement.value = '';
    }
  }

  ngOnDestroy(): void {
    this._endSubscriptions$.next(true);
  }
}
