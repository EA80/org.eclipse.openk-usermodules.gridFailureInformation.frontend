/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
// import { SalutationsResolver } from '@pages/admin/salutations/salutations.resolver';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DistributionGroupListComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-list/distribution-group-list.component';
import { DistributionGroupResolver } from '@grid-failure-information-app/pages/distribution-group/distribution-group.resolver';
import { PublisherGuard } from '@grid-failure-information-app/shared/guards/publisher.guard';

const distributionGroupRoutes: Routes = [
  {
    path: '',
    component: DistributionGroupListComponent,
    canActivate: [PublisherGuard],
    resolve: {
      distributionsGroupTable: DistributionGroupResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(distributionGroupRoutes)],
  exports: [RouterModule],
})
export class DistributionGroupRoutingModule {}
