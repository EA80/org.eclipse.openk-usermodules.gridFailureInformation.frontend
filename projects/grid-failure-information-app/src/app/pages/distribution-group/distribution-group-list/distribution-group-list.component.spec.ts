/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { DistributionGroupListComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-list/distribution-group-list.component';
import { Subscription } from 'rxjs';
import { DistributionGroup } from '@grid-failure-information-app/shared/models';
import { Column, CellClickedEvent, ModelUpdatedEvent, RowNode, GridApi } from 'ag-grid-community';

describe('DistributionGroupListComponent', () => {
  let component: DistributionGroupListComponent;
  let sandbox: DistributionGroupSandbox;

  beforeEach(() => {
    sandbox = {
      registerEvents() {},
      endSubscriptions() {},
      clear() {},
      openForm() {},
      loadDistributionGroupDetail() {},
      getDisplayedRowAtIndex() {},
      deleteDistributionGroup() {},
      showMembersToSelectedGroup() {},
      clearAssignmentInput() {},
    } as any;
    component = new DistributionGroupListComponent(sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define onCellClicked callback', () => {
    component.ngOnInit();
    expect(component.gridOptions.onCellClicked).toBeDefined();
  });

  it('should call appropriate function for delete event', () => {
    const spy: any = spyOn(sandbox, 'deleteDistributionGroup');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'delete', data: new DistributionGroup() });
    expect(spy).toHaveBeenCalled();
  });

  it('should not call deleteDistributionGroup for other events', () => {
    const spy: any = spyOn(sandbox, 'deleteDistributionGroup');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'test', data: new DistributionGroup() });
    expect(spy).not.toHaveBeenCalled();
  });

  it('should call loadDistributionGroupDetail on cellClickedEvent', () => {
    const spy: any = spyOn(sandbox, 'loadDistributionGroupDetail');
    let event: CellClickedEvent = {
      type: 'cellClicked',
      data: new DistributionGroup(),
      column: {
        getColDef() {
          return { field: 'x' };
        },
      },
    } as any;
    component.ngOnInit();
    component.gridOptions.onCellClicked(event);
    expect(spy).toHaveBeenCalled();
  });

  it('should call getDisplayedRowAtIndex on onModelUpdate event in ngOnInit() and select first row.', () => {
    let event = {
      api: {
        getDisplayedRowAtIndex(i: number) {
          return {
            setSelected(j: boolean): void {},
          } as RowNode;
        },
      } as GridApi,
    } as ModelUpdatedEvent;
    component.ngOnInit();
    component.gridOptions.onModelUpdated(event);
    const i = component.api.getSelectedNodes;
    expect(i).toBeUndefined();
  });

  it('should call getDisplayedRowAtIndex on onModelUpdate event in ngOnInit() with selected group', () => {
    sandbox.selectedDistributionGroup = new DistributionGroup({ id: 'xxx' });
    component.api = {
      getDisplayedRowAtIndex(i: number) {
        return {
          setSelected(j: boolean): void {},
        } as RowNode;
      },
    } as any;
    let event: any = {
      api: {
        getDisplayedRowAtIndex(i: number) {
          return {
            setSelected(j: boolean): void {},
          } as RowNode;
        },
        forEachNode() {},
      } as any,
    } as any;
    component.ngOnInit();
    component.gridOptions.onModelUpdated(event);

    expect(component.api.getDisplayedRowAtIndex).toBeDefined();
  });

  it('should call getDisplayRowAtIndex after unselectFirstRow()', () => {
    component.api = {
      getDisplayedRowAtIndex(i: number) {
        return {
          setSelected(j: boolean): void {},
        } as RowNode;
      },
    } as GridApi;

    component.unselectFirstRow();

    expect(component.api.getDisplayedRowAtIndex).toBeDefined();
  });

  it('should not call getDisplayRowAtIndex after unselectFirstRow()', () => {
    component.api = null;
    component.unselectFirstRow();
    expect(component.api).toBeDefined();
  });

  it('should unsubscribe OnDestroy', () => {
    const spy: any = spyOn(sandbox, 'endSubscriptions');
    component['_subscription'] = new Subscription();
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });
});
