/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpService, GET } from '@grid-failure-information-app/shared/async-services/http';
import { Observable } from 'rxjs';

@Injectable()
export class LogoutApiClient extends HttpService {
  /**
   * Logs out from portal
   */
  @GET('/logout')
  public logout(): Observable<any> {
    return null;
  }
}
