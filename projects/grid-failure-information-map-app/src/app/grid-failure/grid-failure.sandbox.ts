/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { GridFailureService } from '@grid-failure-information-map-app/app/grid-failure/grid-failure.service';
import { Subscription, combineLatest } from 'rxjs';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';
import { AppConfigService } from '@grid-failure-information-map-app/app/app-config.service';
import { take } from 'rxjs/operators';
import { GridFailure } from '@grid-failure-information-app/shared/models';
import { VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';

@Injectable()
export class GridFailureSandbox {
  public gridFailureMapList: GridFailure[] = [];
  public mapOptions: MapOptions = new MapOptions();

  private _gridFailureMapListAll: GridFailure[] = [];
  private _gridFailureMapListAllConfigured: GridFailure[] = [];
  private _subscription: Subscription = new Subscription();

  constructor(private _gridFailureService: GridFailureService, private _configService: AppConfigService) {}

  public initSandbox() {
    this._subscription = combineLatest([this._configService.getConfig(), this._gridFailureService.getGridFailureData()])
      .pipe(take(1))
      .subscribe(([config, data]) => {
        this.mapOptions = new MapOptions(config);
        this.mapOptions.extendMarkerInformation = true;
        this.gridFailureMapList = config && config.dataExternInitialVisibility === VisibilityEnum.HIDE ? [] : data;
        this._gridFailureMapListAll = data;
        this._gridFailureMapListAllConfigured = config && config.dataExternInitialVisibility === VisibilityEnum.HIDE ? [] : data;
      });
  }

  public filterGridFailureMapList(postcode: string = '') {
    postcode = postcode.trim();
    this.gridFailureMapList =
      postcode.length > 0
        ? this._gridFailureMapListAll.filter(y => y.postcode === postcode || y.freetextPostcode === postcode)
        : this._gridFailureMapListAllConfigured;
  }

  public unsubscribe() {
    this._subscription.unsubscribe();
  }
}
