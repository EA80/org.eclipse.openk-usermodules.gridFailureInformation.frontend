/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, ModuleWithProviders } from '@angular/core';
import { TableComponent } from '@grid-failure-information-table-app/app/app.component';
import { createCustomElement } from '@angular/elements';
import { AppTableService } from '@grid-failure-information-table-app/app/app-table.service';
import { HttpClientModule } from '@angular/common/http';
import { AgGridModule } from 'ag-grid-angular';
import { PublicApiModule } from '@grid-failure-information-app/app/public-api.module';
import { AppConfigService } from '@grid-failure-information-table-app/app/app-config.service';
import { DatePipe } from '@angular/common';

const providers =[AppTableService, AppConfigService, DatePipe];

@NgModule({
  declarations: [TableComponent],
  imports: [BrowserModule, HttpClientModule, PublicApiModule, AgGridModule.withComponents([])],
  providers: providers,
  entryComponents: [TableComponent],
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const ngElement = createCustomElement(TableComponent, {
      injector: this.injector,
    });
    customElements.define('openk-grid-failure-information-table', ngElement);
  }
}

@NgModule({})
export class TableModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: providers,
    };
  }
}
