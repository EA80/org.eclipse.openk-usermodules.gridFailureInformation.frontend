/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { AppTableService } from '@grid-failure-information-table-app/app/app-table.service';
import { GridFailure, Settings } from '@grid-failure-information-app/shared/models';
import { APP_TABLE_COLDEF } from '@grid-failure-information-table-app/app/app-table-column-definition';
import { GridOptions } from 'ag-grid-community';
import { Globals } from '@grid-failure-information-app/shared/constants/globals';
import { Subscription, combineLatest } from 'rxjs';
import { DatePipe } from '@angular/common';
import { AppConfigService } from '@grid-failure-information-table-app/app/app-config.service';
import { take } from 'rxjs/operators';
import { VisibilityEnum } from '@grid-failure-information-app/shared/constants/enums';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class TableComponent implements OnInit, OnDestroy {
  public Globals = Globals;
  public VisibilityEnum = VisibilityEnum;
  public columnDefs: any;
  public defaultColDef: any;
  public gridOptions: GridOptions;
  public noRowsTemplate: string;
  public gridFailures: GridFailure[];
  public lastModDate: string;

  private _gridApi;
  private _gridFailuresAll: GridFailure[] = [];
  private _gridFailuresAllConfigured: GridFailure[];
  private _subscription: Subscription = new Subscription();

  constructor(private _appTableService: AppTableService, private _configService: AppConfigService, private _datePipe: DatePipe) {
    this.defaultColDef = {
      sortable: true,
      suppressMovable: true,
    };
    this.noRowsTemplate = `<span>Keine Einträge</span>`;
  }
  @Input() set postcode(value: string) {
    value = value.trim();
    let filterFunc = (x: GridFailure) => x.postcode === value || x.freetextPostcode === value;
    if (!!this._gridFailuresAll && this._gridFailuresAll.length > 0) {
      this.gridFailures = value.length > 0 ? this._gridFailuresAll.filter(filterFunc) : this._gridFailuresAllConfigured;
    }
  }

  ngOnInit() {
    this._subscription = combineLatest([this._configService.getConfig(), this._appTableService.loadGridFailureData()])
      .pipe(take(1))
      .subscribe(([config, data]) => {
        if (config && config.visibilityConfiguration) {
          APP_TABLE_COLDEF.forEach((column: any) => {
            column['hide'] = config.visibilityConfiguration.tableExternColumnVisibility[column['field']] === VisibilityEnum.HIDE;
          });
          this.columnDefs = APP_TABLE_COLDEF;
        }
        this.gridFailures = config && config.dataExternInitialVisibility === VisibilityEnum.HIDE ? [] : data;
        this._gridFailuresAll = data;
        this._gridFailuresAllConfigured = config && config.dataExternInitialVisibility === VisibilityEnum.HIDE ? [] : data;
        if (this._datePipe) {
          this.lastModDate = this._datePipe.transform(this._getLastModeDate(), Globals.DATE_TIME_FORMAT);
        }
      });

    this.gridOptions = {
      localeText: Globals.LOCALE_TEXT,
    };
  }

  onGridReady(params): void {
    this._gridApi = params.api;
    const sortModel = [{ colId: 'failureBegin', sort: 'desc' }];
    this._gridApi.setSortModel(sortModel);
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

  private _getLastModeDate(): number {
    if (!!this._gridFailuresAll && this._gridFailuresAll.length > 0) {
      const modeDates: number[] = this._gridFailuresAll.map(gf => Date.parse(gf.modDate));
      return modeDates.sort((a, b) => b - a)[0] || 0;
    }
  }
}
