/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { GridFailureInformationMapComponent } from '@openk-libs/grid-failure-information-map/lib/grid-failure-information-map.component';
import * as L from 'leaflet';
import { async } from '@angular/core/testing';

describe('GridFailureInformationMapComponent', () => {
  let component: GridFailureInformationMapComponent;

  beforeEach(() => {
    component = new GridFailureInformationMapComponent();
    component.setInteractionMode = true;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call _initMap() and _setMarker for overviewMap after calling ngAfterViewInit', async(() => {
    const latitude: any = 123;
    const longitude: any = 456;
    const radius: any = 100;
    const marker: any = {};
    let spyMap: any = spyOn(L, 'map').and.returnValue({ remove() {}, on() {} });
    let spyTileLayer: any = spyOn(L, 'tileLayer').and.returnValue({ addTo() {}, on() {} });
    let spyMarker: any = spyOn(L, 'marker').and.returnValue({
      addTo() {
        return {
          on: () => {
            return { bindTooltip: () => {} };
          },
        };
      },
    });
    let spyCircle: any = spyOn(L, 'circle').and.returnValue({ addTo() {} });

    component.mapData = [{ latitude: latitude, longitude: longitude, radius: radius }];
    component.mapData = [{ latitude: latitude, longitude: longitude, radius: radius }];

    expect(spyMap).toHaveBeenCalled();
    expect(spyTileLayer).toHaveBeenCalled();
    expect(spyMarker).toHaveBeenCalledWith([latitude, longitude], { icon: (component as any)._icon });
    expect(spyCircle).toHaveBeenCalledWith([latitude, longitude], { color: '#204d74', fillColor: '#337ab7', fillOpacity: 0.3, radius: radius });
  }));

  it('should call _initMap() and _setMarker for detailMap after calling ngAfterViewInit', async(() => {
    const latitude: any = 123;
    const longitude: any = 456;
    const radius: any = 100;
    const marker: any = {};
    let spyMap: any = spyOn(L, 'map').and.returnValue({ remove() {}, on() {} });
    let spyTileLayer: any = spyOn(L, 'tileLayer').and.returnValue({ addTo() {}, on() {} });
    let spyMarker: any = spyOn(L, 'marker').and.returnValue({
      addTo() {
        return {
          on: () => {
            return { bindTooltip: () => {} };
          },
        };
      },
    });
    let spyCircle: any = spyOn(L, 'circle').and.returnValue({ addTo() {} });

    component.mapDetailData = { latitude: latitude, longitude: longitude, radius: radius };
    component.mapDetailData = { latitude: latitude };
    component.mapDetailData = { latitude: latitude, longitude: longitude, radius: radius };

    expect(spyMap).toHaveBeenCalled();
    expect(spyTileLayer).toHaveBeenCalled();
    expect(spyMarker).toHaveBeenCalledWith([latitude, longitude], { icon: (component as any)._icon });
  }));

  it('should call _drawPolygonOrCircle(gridFailure)', async(() => {
    const latitude: any = 123;
    const longitude: any = 456;
    const radius: any = 100;
    const polygon: any = {
      value: [
        [2.5, 2.5],
        [2.5, 2.5],
      ],
    };
    let spyCircle: any = spyOn(L, 'circle').and.returnValue({ addTo() {} });
    let spyPolygon: any = spyOn(L, 'polygon').and.returnValue({ addTo() {} });
    const data1 = { latitude: latitude, longitude: longitude, radius: radius };

    (component as any)._drawPolygonOrCircle(data1);
    expect(spyCircle).toHaveBeenCalledWith([latitude, longitude], { color: '#204d74', fillColor: '#337ab7', fillOpacity: 0.3, radius: radius });

    const data2 = { addressPolygonPoints: polygon };
    (component as any)._drawPolygonOrCircle(data2);
    expect(spyPolygon).toHaveBeenCalledWith(polygon.value, { color: '#204d74', fillColor: '#337ab7', fillOpacity: 0.3 });
  }));

  it('should call _emitLatLongValues and emit latlon-values', async(() => {
    let spy: any = spyOn(component.latLong, 'emit');
    const event = { latlng: { lat: 1.24, lng: 1.24 } };
    (component as any)._emitLatLongValues(event);
    expect(spy).toHaveBeenCalledWith({ latitude: 1.24, longitude: 1.24 });
  }));
});
