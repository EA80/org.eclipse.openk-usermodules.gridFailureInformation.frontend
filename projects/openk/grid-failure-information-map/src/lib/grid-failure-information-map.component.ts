/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as L from 'leaflet';
import { Globals } from '@openk-libs/grid-failure-information-map/constants/globals';
import { GridFailureMapInformation } from '@openk-libs/grid-failure-information-map/shared/models/grid-failure-coordinates.model';
import { convertISOToLocalDateTime } from '@openk-libs/grid-failure-information-map/shared/utility/utilityHelpers';
import { MapOptions } from '@openk-libs/grid-failure-information-map/shared/models/map-options.model';
import { determineDetailFieldVisibility } from '@openk-libs/grid-failure-information-map/shared/utility/utilityHelpers';

@Component({
  selector: 'openk-grid-failure-information-map',
  templateUrl: './grid-failure-information-map.component.html',
  styleUrls: ['./grid-failure-information-map.component.scss'],
})
export class GridFailureInformationMapComponent {
  private _mapData: Array<GridFailureMapInformation> = [];
  private _mapDetailData: GridFailureMapInformation;
  private _interactionMode: boolean;
  private _mapOptions: MapOptions = new MapOptions();
  private get _overviewMapInitialLatitude(): number {
    return +this._mapOptions.overviewMapInitialLatitude ? +this._mapOptions.overviewMapInitialLatitude : Globals.OVERVIEW_MAP_INITIAL_LATITUDE;
  }
  private get _overviewMapInitialLongitude(): number {
    return +this._mapOptions.overviewMapInitialLongitude ? +this._mapOptions.overviewMapInitialLongitude : Globals.OVERVIEW_MAP_INITIAL_LONGITUDE;
  }

  @Output() public gridFailureId: EventEmitter<string> = new EventEmitter();

  @Output() latLong: EventEmitter<any> = new EventEmitter();

  @Input()
  set mapOptions(mapOptions: MapOptions) {
    this._mapOptions = mapOptions;
    this._mapOptions.forceResize$.subscribe(() => {
      this.mapDetailData = this._mapDetailData;
    });
  }

  @Input()
  public set mapData(data: Array<any>) {
    this._mapData = [];
    data.forEach(gridFailureData => {
      this._mapData.push(new GridFailureMapInformation(gridFailureData));
    });
    if (!!this._map) {
      this._map.remove();
      this._initMap(this._overviewMapInitialLatitude, this._overviewMapInitialLongitude, this._mapOptions.overviewMapInitialZoom);
    } else {
      this._initMap(this._overviewMapInitialLatitude, this._overviewMapInitialLongitude, this._mapOptions.overviewMapInitialZoom);
    }
    this._setMultipleMarkers();
  }

  @Input()
  public set mapDetailData(gridFailureDetail: any) {
    this._mapDetailData = new GridFailureMapInformation(gridFailureDetail);
    if (!!this._map) {
      this._map.remove();
      if (!!this._mapDetailData.latitude && !!this._mapDetailData.longitude) {
        this._initMap(this._mapDetailData.latitude, this._mapDetailData.longitude, this._mapOptions.detailMapInitialZoom);
      } else {
        this._initMap(this._overviewMapInitialLatitude, this._overviewMapInitialLongitude, this._mapOptions.detailMapInitialZoom);
      }
    } else {
      this._initMap(this._overviewMapInitialLatitude, this._overviewMapInitialLongitude, this._mapOptions.detailMapInitialZoom);
    }
    this._setDetailMarker();
  }

  @Input()
  public set setInteractionMode(interactionMode: boolean) {
    this._interactionMode = interactionMode;
  }

  private _map: any;
  private _icon = L.icon({
    iconUrl: './marker-icon.png',
    iconSize: [Globals.ICON_SIZE_X_COORDINATE, Globals.ICON_SIZE_Y_COORDINATE],
    iconAnchor: [Globals.ICON_ANCHOR_X_COORDINATE, Globals.ICON_ANCHOR_Y_COORDINATE],
  });

  constructor() {}

  private _initMap(latitude: number, longitude: number, zoom: number): void {
    this._map = L.map(Globals.MAP_ID, {
      center: [latitude, longitude],
      zoom: zoom,
    });
    const tiles = L.tileLayer(Globals.TITLE_LAYER, {
      maxZoom: Globals.MAX_ZOOM,
      attribution: Globals.TITLE_ATTRIBUTION,
      crossOrigin: null,
    });
    tiles.on('load', () => {
      this._map.invalidateSize();
    });
    tiles.addTo(this._map);

    this._map.on('click', this._emitLatLongValues.bind(this));
  }

  private _setMultipleMarkers(): void {
    if (!!this._map && !!this._mapData && this._mapOptions) {
      this._mapData.forEach(gridFailure => {
        if (gridFailure.latitude && gridFailure.longitude) {
          const currentMarker = L.marker([gridFailure.latitude, gridFailure.longitude], { icon: this._icon })
            .addTo(this._map)
            .on('click', () => this.gridFailureId.emit(gridFailure.id));
          let failureBeginVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'failureBegin');
          let failureBeginString = failureBeginVisibility
            ? `${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_BEGIN}${Globals.STRONG_END_TAG} ${
                !!gridFailure.failureBegin ? convertISOToLocalDateTime(gridFailure.failureBegin) : ''
              }${Globals.BREAK_TAG}`
            : '';

          let failureEndPlannedVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'failureEndPlanned');
          let failureEndPlannedString = failureEndPlannedVisibility
            ? ` ${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_END_PLANNED}${Globals.STRONG_END_TAG} ${
                !!gridFailure.failureEndPlanned ? convertISOToLocalDateTime(gridFailure.failureEndPlanned) : ''
              } ${Globals.BREAK_TAG}`
            : '';

          let expectedReasonVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'expectedReasonText');
          let expectedReasonString = expectedReasonVisibility
            ? `  ${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_EXPECTED_REASON}${Globals.STRONG_END_TAG} ${gridFailure.expectedReasonText || ''} ${
                Globals.BREAK_TAG
              }`
            : '';

          let branchVisibility =
            !this._mapOptions.extendMarkerInformation ||
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'branch');
          let branchString = branchVisibility
            ? `  ${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_BRANCH}${Globals.STRONG_END_TAG} ${gridFailure.branchDescription || ''}`
            : '';

          /* adress fields: only if extendMarkerInformation is true */
          let postcodeVisibility =
            this._mapOptions.extendMarkerInformation &&
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'postcode');
          let postcodeString = postcodeVisibility
            ? `  ${Globals.BREAK_TAG}${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_ZIP}${Globals.STRONG_END_TAG} ${
                gridFailure.postcode || gridFailure.freetextPostcode || ''
              }`
            : '';
          let cityVisibility =
            this._mapOptions.extendMarkerInformation &&
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'city');
          let cityString = cityVisibility
            ? ` ${Globals.BREAK_TAG}${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_CITY}${Globals.STRONG_END_TAG} ${
                gridFailure.city || gridFailure.freetextCity || ''
              }`
            : '';

          let districtVisibility =
            this._mapOptions.extendMarkerInformation &&
            determineDetailFieldVisibility(this._mapOptions.visibilityConfiguration, 'mapExternTooltipVisibility', 'district');
          let districtString = districtVisibility
            ? `  ${Globals.BREAK_TAG}${Globals.STRONG_BEGIN_TAG}${Globals.GRID_FAILURE_DISTRICT}${Globals.STRONG_END_TAG} ${
                gridFailure.district || gridFailure.freetextDistrict || ''
              }`
            : '';

          /* tooltip assembly */
          let tooltipContent = `${failureBeginString}${failureEndPlannedString}${expectedReasonString}${branchString}${postcodeString}${cityString}${districtString}`;
          currentMarker.bindTooltip(tooltipContent);
          this._drawPolygonOrCircle(gridFailure);
        }
      });
    }
  }

  private _setDetailMarker(): void {
    if (!!this._map && !!this._mapDetailData) {
      if (this._mapDetailData.latitude && this._mapDetailData.longitude) {
        L.marker([this._mapDetailData.latitude, this._mapDetailData.longitude], { icon: this._icon }).addTo(this._map);
        this._drawPolygonOrCircle(this._mapDetailData);
      }
    }
  }

  // Draw whether polygon, circle or nothing; graded according to priority
  private _drawPolygonOrCircle(gridFailure: GridFailureMapInformation): void {
    if (!!gridFailure.addressPolygonPoints && !!gridFailure.addressPolygonPoints['value'] && !!gridFailure.addressPolygonPoints['value'].length) {
      L.polygon(gridFailure.addressPolygonPoints['value'], {
        color: Globals.RADIUS_BORDER_COLOR,
        fillColor: Globals.RADIUS_FILL_COLOR,
        fillOpacity: Globals.RADIUS_FILL_OPACITY,
      }).addTo(this._map);
    } else if (!!gridFailure.addressPolygonPoints && !!gridFailure.addressPolygonPoints.length) {
      L.polygon(gridFailure.addressPolygonPoints, {
        color: Globals.RADIUS_BORDER_COLOR,
        fillColor: Globals.RADIUS_FILL_COLOR,
        fillOpacity: Globals.RADIUS_FILL_OPACITY,
      }).addTo(this._map);
    } else if (!!gridFailure.radius) {
      L.circle([gridFailure.latitude, gridFailure.longitude], {
        color: Globals.RADIUS_BORDER_COLOR,
        fillColor: Globals.RADIUS_FILL_COLOR,
        fillOpacity: Globals.RADIUS_FILL_OPACITY,
        radius: gridFailure.radius,
      }).addTo(this._map);
    }
  }

  private _emitLatLongValues(event) {
    if (this._interactionMode) {
      const latLonValues = { latitude: event.latlng.lat, longitude: event.latlng.lng };
      this.latLong.emit(latLonValues);
    }
  }
}
