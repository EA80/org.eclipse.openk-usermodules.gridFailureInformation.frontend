/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export interface VisibilityConfigurationInterface {
  fieldVisibility: {
    description: string;
    failureClassification: string;
    internalRemark: string;
    responsibility: string;
  };
  mapExternTooltipVisibility: {
    branch: string;
    city: string;
    district: string;
    expectedReasonText: string;
    failureBegin: string;
    failureEndPlanned: string;
    postcode: string;
  };
  tableExternColumnVisibility: {
    branch: string;
    city: string;
    description: string;
    district: string;
    expectedReasonText: string;
    failureBegin: string;
    failureClassification: string;
    failureEndPlanned: string;
    postcode: string;
    street: string;
  };
  tableInternColumnVisibility: {
    branch: string;
    city: string;
    description: string;
    district: string;
    expectedReasonText: string;
    failureBegin: string;
    failureClassification: string;
    failureEndPlanned: string;
    failureEndResupplied: string;
    housenumber: string;
    internalRemark: string;
    postcode: string;
    pressureLevel: string;
    publicationStatus: string;
    radius: string;
    responsibility: string;
    stationIds: string;
    statusExtern: string;
    statusIntern: string;
    street: string;
    voltageLevel: string;
  };
}
