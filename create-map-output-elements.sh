#!/bin/sh
echo Starting...
echo

dist_path=./dist/grid-failure-information-map-app

cd $dist_path
echo navigate to $dist_path

rm -rf outputElements.tar.gz
echo old outputElements.tar.gz file removed
echo

mkdir outputElements
echo outputElements folder created
echo

cp  main*.js ./outputElements
echo main files copied

cp  runtime*.js ./outputElements
echo runtime files copied

cp  polyfills*.js ./outputElements
echo polyfills files copied

cp  scripts*.js ./outputElements
echo scripts files copied

cp  styles*.css ./outputElements
echo styles files copied

cp  index.html ./outputElements
echo index file copied
echo

tar -czf outputElements.tar.gz outputElements
echo outputElements moved in tar
echo

rm -rf outputElements
echo temporal outputElements file removed
echo

echo Done!
